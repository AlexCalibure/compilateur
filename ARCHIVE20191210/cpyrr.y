 %{
    #include <stdio.h>
    #include "declarations.h"
    #include "representation.h"
    #include "arbre_abstrait.h"
    extern int yyerror();
    extern int yylex();
    extern int numligne;
    extern int region;
    extern int nbr_champs;
    extern int nbr_dimensions;
    extern int nbr_params;
    extern int champs[];
    extern int dimensions[];
    extern int params[];
    extern int indice;
    
%}

%union {
    struct{
        arbre a;
        int type;
    }struct_arbre;
    int valeur_defaut;
}

%token PROG PV DEBUT FIN TYPE  DP STRUCT FSTRUCT TABLEAU FLECHE ENTIER REEL BOOLEEN CARACTERE  RETOURNE PO PF VIDE SI ALORS SINON TANT_QUE FAIRE OPAFF CO CF P V  OU ET NON VRAI FAUX OPEG OPDIF OPINF OPSUP OPINFEG OPSUPEG PLUS MOINS MULT DIV MOD NEG VARIABLE PROCEDURE FONCTION BEG END P_P

%token <valeur_defaut> IDF CSTE_ENTIERE CSTE_REELE CHAINE UN_CARACTERE


%type <struct_arbre> corps liste_instructions instruction affectation condition tant_que appel variable resultat_retourne liste_arguments liste_args un_arg liste_indices  var_struct suite_variable expression comparaison eb eb1 eb2 eb3 ea ea1 ea2

%type <valeur_defaut> liste_declarations liste_declarations_type liste_declarations_variable liste_declarations_pf declaration_type declaration_variable declaration_pf declaration_procedure declaration_fonction suite_declaration_type liste_champs un_champ liste_dimensions une_dimension nom_type type_simple 

%%

programme : PROG  BEG corps END 
          ;

corps : liste_declarations liste_instructions {$$.a = $2.a;}
      ;

liste_declarations : liste_declarations_type liste_declarations_variable liste_declarations_pf
                   ;

liste_declarations_type : liste_declarations_type declaration_type PV
                        | {;}
                        ;

liste_declarations_variable : liste_declarations_variable declaration_variable PV
                            | {;}
                            ;

liste_declarations_pf : liste_declarations_pf declaration_pf PV 
                      | {;}
                      ;

liste_instructions : instruction PV {$$.a = concat_pere_fils(creer_noeud(A_LISTE_INST,EMPTY,EMPTY),$1.a);}
                   | liste_instructions instruction PV {$$.a = concat_fils_frere($1.a,concat_pere_fils(creer_noeud(A_LISTE_INST,EMPTY,EMPTY),$1.a));}
                   ;

declaration_pf : declaration_fonction 
               | declaration_procedure
               | {;}
               ;


declaration_type : TYPE IDF DP suite_declaration_type {
    if ($4 == STRCT){
        indice = ajouter_declaration($2,$4,sommet(p),ajouter_structure(nbr_champs/3,champs));
    }
      else{
          int tab = ajouter_tableaux($4,nbr_dimensions/2,dimensions);
          printf("TAB %d %d %d\n",$2,sommet(p),tab);
          indice = ajouter_declaration($2,TAB,sommet(p),tab);
          }
      nbr_dimensions = 0;
      nbr_champs = 0;}
                 ;

suite_declaration_type : STRUCT liste_champs FSTRUCT {$$ = STRCT;}
                       | TABLEAU dimension FLECHE nom_type {$$ = $4;}
                       ;

dimension : CO liste_dimensions CF
          ;

liste_dimensions : une_dimension 
                 | liste_dimensions V une_dimension 
                 ;

/* Nous avons vu ce problème en TD... il faut remplacer P P par P_P
pour éviter un conflit par exemple lorsqu'on doit analyser
x.ch1..x.ch2 (relisez le TD concerné)
une_dimension : expression P P expression
              ;
*/

une_dimension : ea P_P ea {dimensions[nbr_dimensions++] = $1.type; dimensions[nbr_dimensions++] = $3.type; $$ = $3.type - $1.type;}
              ;

liste_champs : un_champ 
             | liste_champs PV un_champ
             ;

un_champ : IDF DP nom_type {champs[nbr_champs++] = $3; champs[nbr_champs++] = $1; champs[nbr_champs++] = 1; $$ = $3; }
         ;

nom_type : type_simple {$$ = $1;}
         | IDF {$$ = assoc_nom($1);}
         ;

type_simple : ENTIER {$$ = 0;}
            | REEL {$$ = 1;}
            | BOOLEEN {$$ = 3;}
            | CARACTERE {$$ = 2;}
            | CHAINE CO CSTE_ENTIERE CF {;}
            ;

declaration_variable : VARIABLE IDF DP nom_type {indice = ajouter_declaration($2,VAR,sommet(p),assoc_nom($4),assoc_nom($4));modifier_taille_region(region,tab_declarations[indice].execution);}
                     ;

declaration_procedure : PROCEDURE IDF liste_parametres DEBUT corps FIN {printf("Procedure -> %d\n",region);indice = ajouter_declaration($2,PROC,sommet(p),ajouter_procedure(nbr_params/2,params),region);nbr_params = 0; }
                      ;

declaration_fonction : FONCTION IDF liste_parametres RETOURNE type_simple DEBUT corps FIN {printf("Fonction -> %d\n",region);indice = ajouter_declaration($2,FCT,sommet(p),ajouter_fonction($5,nbr_params/2,params),region);nbr_params=0;}
                     ;

liste_parametres :
                 | PO liste_param PF
		 ;
		 
liste_param : un_param
            | liste_param PV un_param
            ;

un_param : IDF DP type_simple {params[nbr_params++] = $1; params[nbr_params++] = $3;indice = ajouter_declaration($1,PARAM,sommet(p),assoc_nom($3),$3);}
         ;

instruction : affectation {$$.a = $1.a;}
            | condition   {$$.a = $1.a;}
            | tant_que    {$$.a = $1.a;}
            | appel       {$$.a = $1.a;}
            | VIDE        {$$.a = creer_noeud(A_VIDE,EMPTY,EMPTY);}
            | RETOURNE resultat_retourne {$$.a = concat_pere_fils(creer_noeud(A_RETURN,EMPTY,EMPTY),$2.a);}
            ;


resultat_retourne : {$$.a = creer_arbre_vide();}
                  | expression { $$.a = $1.a;}
		  ;


/* si l'on écrit appel comme vous l'indiquez, cela veut dire qu'on
peut écrire "p" pour appeler une procédure comme instruction
mais comme vous utilisez également appel dans les expressions arithmétique,
lorsqu'on utilise p dans une expression comme (x+2*p)+3 par exemple, et bien l'analyseur syntaxique ne sait pas si p est une variable ou un appel de fonction sans paramètre. Pour solutionner cela, il suffit de dire qu'un appel de fonction, ou de procédure, doit toujours avoir une parenthèse ouvrante et une parenthèse fermante, comme en C. Si on veut appeler un procedure ou fonction p sans paramètre on ndoit écrire p(). Du coup il faut changer la règle de réécriture pour liste_arguments et liste_args */

appel : IDF liste_arguments {if (tab_declarations[assoc_nom($1)].nature == FCT ){
                                  concat_pere_fils(creer_noeud(A_FONCTION,EMPTY,EMPTY),$2.a);
                             }
                             else if(tab_declarations[assoc_nom($1)].nature == PROC){
                                  concat_pere_fils(creer_noeud(A_PROCEDURE,EMPTY,EMPTY),$2.a);
                             }
                             else{
                                 printf("L'appel n'est pas fait sur une fonction ou procedure déclaré ligne %d\n",numligne);
                             }
  }
      ;

/* votre version
liste_arguments : 
                | PO liste_args PF
    ;

liste_args : un_arg
           | liste_args V un_arg
           ;

*/

/* la version modifiée */
liste_arguments : PO liste_args PF {$$.a = $2.a;}
    ;

liste_args :  {$$.a = creer_arbre_vide();}
           | un_arg {concat_pere_fils(creer_noeud(A_LISTE_ARGS,EMPTY,EMPTY),$1.a);}
           | liste_args V un_arg {concat_fils_frere($1.a,concat_pere_fils(creer_noeud(A_LISTE_ARGS,EMPTY,EMPTY),$3.a));}
           ;

un_arg : expression {$$.a = $1.a;}
       ;

condition : SI eb ALORS liste_instructions SINON liste_instructions {concat_pere_fils(creer_noeud(A_IF_THEN_ELSE,EMPTY,EMPTY),concat_fils_frere($2.a,concat_fils_frere($4.a,$6.a)));}
          ;

/* comme pour la condition il faut utiliser eb et non pas expression
tant_que : TANT_QUE expression FAIRE liste_instructions
         ;
*/

tant_que : TANT_QUE eb FAIRE liste_instructions {concat_pere_fils(creer_noeud(A_WHILE,EMPTY,EMPTY),concat_fils_frere($2.a,$4.a));}
         ;

affectation : variable OPAFF expression {concat_pere_fils(creer_noeud(A_OPAFF,EMPTY,EMPTY),concat_fils_frere($1.a,$3.a));}
            ;

variable : IDF suite_variable {concat_pere_fils(creer_noeud(A_IDF,$1,assoc_nom($1)),$2.a);}
         ;

suite_variable : CO liste_indices CF var_struct {
    if (est_vide($4.a)){
        $$.a = concat_pere_fils(creer_noeud(A_TAB,EMPTY,EMPTY),$2.a);
    }
    else{
        $$.a = concat_pere_fils(creer_noeud(A_STRUCT,EMPTY,EMPTY),concat_fils_frere(creer_noeud(A_TAB,EMPTY,EMPTY),$4.a));
    }
  }
               | var_struct   {$$.a = creer_noeud(A_LISTE_CHAMPS,EMPTY,EMPTY);}
	       ;

var_struct : P variable {$$.a = $2.a;}
           | {$$.a = creer_arbre_vide();}
           ;

liste_indices : ea {$$.a = concat_pere_fils(creer_noeud(A_LISTE_INDICE,EMPTY,EMPTY),$1.a);}
              | liste_indices V ea {$$.a = concat_pere_fils($1.a,concat_fils_frere(creer_noeud(A_LISTE_INDICE,EMPTY,EMPTY),$3.a));}
              ;


expression : ea {$$.type = $1.type; $$.a = $1.a;}
           | eb {$$.type = $1.type; $$.a = $1.a;}
           ;

eb : eb OU eb1   {$$.a = concat_pere_fils(creer_noeud(A_OU,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
   | eb1         {$$.type = $1.type; $$.a = $1.a;}
   ;

eb1 : eb1 ET eb2 {$$.a = concat_pere_fils(creer_noeud(A_ET,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
    | eb2        {$$.type = $1.type; $$.a = $1.a;}
    ;

eb2 : NON eb3 {$$.a = concat_pere_fils(creer_noeud(A_NON,EMPTY,EMPTY), $2.a);}
    | eb3     {$$.type = $1.type; $$.a = $1.a;}
    ;

eb3 : PO eb PF     {$$.a = $2.a;}
    | VRAI         {$$.a = creer_noeud(A_VRAI,EMPTY,EMPTY);}
    | FAUX         {$$.a = creer_noeud(A_FAUX,EMPTY,EMPTY);}
    | comparaison  {$$.type = $1.type; $$.a = $1.a;}
    ;


comparaison : ea OPEG ea    {$$.a = concat_pere_fils(creer_noeud(A_OPEG,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
            | ea OPDIF ea   {$$.a = concat_pere_fils(creer_noeud(A_OPDIF,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
            | ea OPINF ea   {$$.a = concat_pere_fils(creer_noeud(A_OPINF,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
            | ea OPSUP ea   {$$.a = concat_pere_fils(creer_noeud(A_OPSUP,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
            | ea OPINFEG ea {$$.a = concat_pere_fils(creer_noeud(A_OPINFEG,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
            | ea OPSUPEG ea {$$.a = concat_pere_fils(creer_noeud(A_OPSUPEG,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
            ;

ea : ea PLUS ea1 {$$.a = concat_pere_fils(creer_noeud(A_PLUS,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
   | ea MOINS ea1 {$$.a = concat_pere_fils(creer_noeud(A_MOINS,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
   | ea1 {$$.type = $1.type; $$.a = $1.a;}
   ;

ea1 : ea1 MULT ea2 {$$.a = concat_pere_fils(creer_noeud(A_MULT,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
    | ea1 DIV ea2 {$$.a = concat_pere_fils(creer_noeud(A_DIV,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
    | ea1 MOD ea2 {$$.a = concat_pere_fils(creer_noeud(A_MOD,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
    | ea2 {$$.type = $1.type; $$.a = $1.a;}
    ;

ea2 : PO ea PF       {$$.type = $2.type; $$.a = $2.a;}
    | CSTE_ENTIERE   {$$.type= $1; $$.a = creer_noeud(A_INT, EMPTY, EMPTY); printf("expression %d",$1);}
    | CSTE_REELE     {$$.type = $1; $$.a =  creer_noeud(A_FLOAT,EMPTY, EMPTY);}
    | CHAINE         {$$.type = $1; $$.a = creer_noeud(A_CHAINE,EMPTY,EMPTY);}
    | UN_CARACTERE   {$$.type = $1; $$.a = creer_noeud(A_CHAR,EMPTY,EMPTY);}
    | variable       {$$.a = $1.a;}
    | appel          {$$.a = $1.a;}
    ;


%%

int yyerror(){
printf("Erreur de syntaxe en ligne %d\n", numligne);
}
