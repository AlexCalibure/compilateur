 %{
    #include <stdio.h>
    #include "declarations.h"
    extern int yyerror();
    extern int yylex();
    extern int yylval;
    extern int numligne;
    extern int region;
%}

%token PROG PV DEBUT FIN TYPE IDF DP STRUCT FSTRUCT TABLEAU FLECHE ENTIER REEL BOOLEEN CARACTERE CHAINE CSTE_ENTIERE CSTE_REELE RETOURNE PO PF VIDE SI ALORS SINON TANT_QUE FAIRE OPAFF CO CF P V  OU ET NON VRAI FAUX OPEG OPDIF OPINF OPSUP OPINFEG OPSUPEG PLUS MOINS MULT DIV MOD NEG VARIABLE PROCEDURE FONCTION UN_CARACTERE BEG END

%token P_P
%%

programme : PROG  BEG corps END
          ;

corps : liste_declarations liste_instructions
      |  liste_instructions 
      ;

liste_declarations : declaration PV
                   | liste_declarations declaration PV
                   ;

liste_instructions : suite_liste_inst
                   ;

suite_liste_inst : instruction PV
                 | suite_liste_inst instruction PV
                 ;

declaration : declaration_type 
            | declaration_variable
            | declaration_procedure
            | declaration_fonction
            ;

declaration_type : TYPE IDF DP suite_declaration_type {ajouter_declaration($2,$1,region);}
                 ;

suite_declaration_type : STRUCT liste_champs FSTRUCT {$$ = STRCT;}
                       | TABLEAU dimension FLECHE nom_type {$$ = TAB;}
                       ;

dimension : CO liste_dimensions CF
          ;

liste_dimensions : une_dimension
                 | liste_dimensions V une_dimension
                 ;

/* Nous avons vu ce problème en TD... il faut remplacer P P par P_P
pour éviter un conflit par exemple lorsqu'on doit analyser
x.ch1..x.ch2 (relisez le TD concerné)
une_dimension : expression P P expression
              ;
*/

une_dimension : expression P_P expression
              ;

liste_champs : un_champ
             | liste_champs PV un_champ
             ;

un_champ : IDF DP nom_type
         ;

nom_type : type_simple
         | IDF
         ;

type_simple : ENTIER
            | REEL
            | BOOLEEN
            | CARACTERE
            | CHAINE CO CSTE_ENTIERE CF
            ;

declaration_variable : VARIABLE IDF DP nom_type {ajouter_declaration($2,VAR,region);}
                     ;

declaration_procedure : PROCEDURE IDF liste_parametres DEBUT corps FIN {ajouter_declaration($2,PROC,region);}
                      ;

declaration_fonction : FONCTION IDF liste_parametres RETOURNE type_simple DEBUT corps FIN  {ajouter_declaration($2,FCT,region);}
                     ;

liste_parametres :
                 | PO liste_param PF
		 ;
		 
liste_param : un_param
            | liste_param PV un_param
            ;

un_param : IDF DP type_simple
         ;

instruction : affectation
            | condition
            | tant_que
            | appel
            | VIDE
            | RETOURNE resultat_retourne
            ;


resultat_retourne :
                  | expression
		  ;

/* si l'on écrit appel comme vous l'indiquez, cela veut dire qu'on
peut écrire "p" pour appeler une procédure comme instruction
mais comme vous utilisez également appel dans les expressions arithmétique,
lorsqu'on utilise p dans une expression comme (x+2*p)+3 par exemple, et bien l'analyseur syntaxique ne sait pas si p est une variable ou un appel de fonction sans paramètre. Pour solutionner cela, il suffit de dire qu'un appel de fonction, ou de procédure, doit toujours avoir une parenthèse ouvrante et une parenthèse fermante, comme en C. Si on veut appeler un procedure ou fonction p sans paramètre on ndoit écrire p(). Du coup il faut changer la règle de réécriture pour liste_arguments et liste_args */

appel : IDF liste_arguments
      ;

/* votre version
liste_arguments : 
                | PO liste_args PF
    ;

liste_args : un_arg
           | liste_args V un_arg
           ;

*/

/* la version modifiée */
liste_arguments : PO liste_args PF
    ;

liste_args : 
           | un_arg
           | liste_args V un_arg
           ;

un_arg : expression
       ;

condition : SI eb ALORS liste_instructions SINON liste_instructions
          ;

/* comme pour la condition il faut utiliser eb et non pas expression
tant_que : TANT_QUE expression FAIRE liste_instructions
         ;
*/

tant_que : TANT_QUE eb FAIRE liste_instructions
         ;

affectation : variable OPAFF expression
            ;

variable : IDF suite_variable
         ;

suite_variable : CO liste_indices CF var_struct
               | var_struct
	       ;

var_struct : P variable
           |
           ;

liste_indices : ea
              | liste_indices V ea
              ;


expression : ea
           | eb
           ;

eb : eb OU eb1
   | eb1
   ;

eb1 : eb1 ET eb2
    | eb2
    ;

eb2 : NON eb3
    | eb3
    ;

eb3 : PO eb PF
    | VRAI
    | FAUX
    | comparaison
    ;


comparaison : ea OPEG ea
            | ea OPDIF ea
            | ea OPINF ea
            | ea OPSUP ea
            | ea OPINFEG ea
            | ea OPSUPEG ea
            ;

ea : ea PLUS ea1
   | ea MOINS ea1
   | ea1
   ;

ea1 : ea1 MULT ea2
    | ea1 DIV ea2
    | ea1 MOD ea2
    | ea2
    ;

ea2 : PO ea PF
    | CSTE_ENTIERE
    | CSTE_REELE
    | CHAINE
    | UN_CARACTERE
    | variable
    | appel
    ;


%%

int yyerror(){
printf("Erreur de syntaxe en ligne %d\n", numligne);
}
