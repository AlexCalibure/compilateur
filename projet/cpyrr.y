 %{
    #include <stdio.h>
     #include<string.h>
    #include "declarations.h"
    #include "representation.h"
    #include "arbre_abstrait.h"
    #include "table_lexico.h"

     #define T_ENTIER 0
     #define T_REEL 1
     #define T_CHAR 2
     #define T_STRING -3
     #define T_BOOL 3


     #define MAX_ARG 50
     #define MAX_DIM 50
     
     extern int yyerror();
     extern int yylex();
     int verife_arg_fonction(int numelexico);
     int verifier_dimensions(int num_dec_type);
     extern int numligne;
     extern int region;
     extern int nbr_champs;
     extern int nbr_dimensions;
     extern int nbr_params;
     extern int champs[];
     extern int dimensions[];
     extern int params[];
     extern int indice;
     int test_assoc;
     int nb_arg_fonction = 0;
     int arg_fonction[MAX_ARG];
     int verif_dimensions[MAX_DIM];
     int nb_dim_verif = 0;
     int decl_associe;
%}

%union {
    struct{
        arbre a;
        int type;
        union{
            int i;
            char c;
            float f;
            char *s;
        }valeur;
    }struct_arbre;
    struct{
        int type;
        union{
            int i;
            char c;
            float f;
            char *s;
        }valeur;
    }struct_simple;
}

%token PROG PV DEBUT FIN TYPE  DP STRUCT FSTRUCT TABLEAU FLECHE ENTIER REEL BOOLEEN CARACTERE  RETOURNE PO PF VIDE SI ALORS SINON TANT_QUE FAIRE OPAFF CO CF P V  OU ET NON VRAI FAUX OPEG OPDIF OPINF OPSUP OPINFEG OPSUPEG PLUS MOINS MULT DIV MOD NEG VARIABLE PROCEDURE FONCTION BEG END P_P

%token <struct_simple> IDF CSTE_ENTIERE CSTE_REELE CHAINE UN_CARACTERE

%type <struct_arbre> corps liste_instructions instruction affectation condition tant_que appel variable resultat_retourne liste_arguments liste_args un_arg liste_indices  var_struct suite_variable expression comparaison eb eb1 eb2 eb3 ea ea1 ea2

%type <struct_simple> liste_declarations liste_declarations_type liste_declarations_variable liste_declarations_pf declaration_type declaration_variable declaration_pf declaration_procedure declaration_fonction suite_declaration_type liste_champs un_champ liste_dimensions une_dimension nom_type type_simple 

%%

programme : PROG  BEG corps END 
          ;

corps : liste_declarations liste_instructions {$$.a = $2.a;}
      ;

liste_declarations : liste_declarations_type liste_declarations_variable liste_declarations_pf
                   ;

liste_declarations_type : liste_declarations_type declaration_type PV
                        | {;}
                        ;

liste_declarations_variable : liste_declarations_variable declaration_variable PV
                            | {;}
                            ;

liste_declarations_pf : liste_declarations_pf declaration_pf PV 
                      | {;}
                      ;

liste_instructions : instruction PV {$$.a = concat_pere_fils(creer_noeud(A_LISTE_INST,EMPTY,EMPTY),$1.a);}
                   | liste_instructions instruction PV {$$.a = concat_fils_frere($1.a,concat_pere_fils(creer_noeud(A_LISTE_INST,EMPTY,EMPTY),$1.a));}
                   ;

declaration_pf : declaration_fonction 
               | declaration_procedure
               | {;}
               ;


declaration_type : TYPE IDF DP suite_declaration_type {
    if ($4.valeur.i == STRCT){
        indice = ajouter_declaration($2.valeur.i,$4.valeur.i,sommet(p),ajouter_structure(nbr_champs/3,champs));
    }
    else{
        int tab = ajouter_tableaux($4.type,nbr_dimensions/2,dimensions);
        indice = ajouter_declaration($2.valeur.i,TAB,sommet(p),tab);
    }
      nbr_dimensions = 0;
      nbr_champs = 0;}
                 ;

suite_declaration_type : STRUCT liste_champs FSTRUCT {$$.valeur.i = STRCT;}
                       | TABLEAU dimension FLECHE nom_type {$$.type = $4.type ,$$.valeur.i = $4.valeur.i;}
                       ;

dimension : CO liste_dimensions CF
          ;

liste_dimensions : une_dimension 
                 | liste_dimensions V une_dimension 
                 ;

/* Nous avons vu ce problème en TD... il faut remplacer P P par P_P
pour éviter un conflit par exemple lorsqu'on doit analyser
x.ch1..x.ch2 (relisez le TD concerné)
une_dimension : expression P P expression
              ;
*/

une_dimension : ea P_P ea {dimensions[nbr_dimensions++] = $1.valeur.i; dimensions[nbr_dimensions++] = $3.valeur.i; $$.valeur.i = $3.valeur.i - $1.valeur.i;}
              ;

liste_champs : un_champ 
             | liste_champs PV un_champ
             ;

un_champ : IDF DP nom_type {champs[nbr_champs++] = $3.type; champs[nbr_champs++] = $1.valeur.i; champs[nbr_champs++] = 1; $$.valeur.i = $3.type; }
         ;

nom_type : type_simple {$$.type = $1.type;}
         | IDF {if((test_assoc = assoc_nom($1.valeur.i)) != -1 ) $$.type = assoc_nom(test_assoc); else printf("Erreur : Le type %d n'existe pas ligne %d\n",$1.valeur.i,numligne);}
         ;

type_simple : ENTIER {$$.type = 0;}
            | REEL {$$.type = 1;}
            | BOOLEEN {$$.type = 3;}
            | CARACTERE {$$.type = 2;}
            | CHAINE CO CSTE_ENTIERE CF {;}
            ;

declaration_variable : VARIABLE IDF DP nom_type { indice = ajouter_declaration($2.valeur.i,VAR,sommet(p),assoc_nom($4.type),assoc_nom($4.type)); modifier_taille_region(region,tab_declarations[indice].execution);}
                     ;

declaration_procedure : PROCEDURE IDF liste_parametres DEBUT corps FIN {indice = ajouter_declaration($2.valeur.i,PROC,sommet(p),ajouter_procedure(nbr_params/2,params),region);nbr_params = 0;}
                      ;

declaration_fonction : FONCTION IDF liste_parametres RETOURNE type_simple DEBUT corps FIN {indice = ajouter_declaration($2.valeur.i,FCT,sommet(p),ajouter_fonction($5.type,nbr_params/2,params),region);nbr_params=0;}
                     ;

liste_parametres :
                 | PO liste_param PF
		 ;
		 
liste_param : un_param
            | liste_param PV un_param
            ;

un_param : IDF DP type_simple {params[nbr_params++] = $1.valeur.i; params[nbr_params++] = $3.type; indice = ajouter_declaration($1.valeur.i,PARAM,sommet(p),assoc_nom($3.type),$3.type);}
         ;

instruction : affectation {$$.a = $1.a;}
            | condition   {$$.a = $1.a;}
            | tant_que    {$$.a = $1.a;}
            | appel       {$$.a = $1.a; memset(arg_fonction,-1,sizeof(int)*MAX_ARG);nb_arg_fonction;}
            | VIDE        {$$.a = creer_noeud(A_VIDE,EMPTY,EMPTY);}
            | RETOURNE resultat_retourne {$$.a = concat_pere_fils(creer_noeud(A_RETURN,EMPTY,EMPTY),$2.a);}
            ;


resultat_retourne : {$$.a = creer_arbre_vide();}
                  | expression { $$.a = $1.a;}
		  ;


/* si l'on écrit appel comme vous l'indiquez, cela veut dire qu'on
peut écrire "p" pour appeler une procédure comme instruction
mais comme vous utilisez également appel dans les expressions arithmétique,
lorsqu'on utilise p dans une expression comme (x+2*p)+3 par exemple, et bien l'analyseur syntaxique ne sait pas si p est une variable ou un appel de fonction sans paramètre. Pour solutionner cela, il suffit de dire qu'un appel de fonction, ou de procédure, doit toujours avoir une parenthèse ouvrante et une parenthèse fermante, comme en C. Si on veut appeler un procedure ou fonction p sans paramètre on ndoit écrire p(). Du coup il faut changer la règle de réécriture pour liste_arguments et liste_args */

appel : IDF liste_arguments {
    if (assoc_nom($1.valeur.i) != -1){
        $$.type = representations[tab_declarations[assoc_nom($1.valeur.i)].description];
        $$.a = creer_arbre_vide();
        if (verife_arg_fonction($1.valeur.i) ){
            if (tab_declarations[assoc_nom($1.valeur.i)].nature == FCT ){
                $$.a = concat_pere_fils(creer_noeud(A_FONCTION,EMPTY,EMPTY),$2.a);
            }
            else if(tab_declarations[assoc_nom($1.valeur.i)].nature == PROC){
                $$.a = concat_pere_fils(creer_noeud(A_PROCEDURE,EMPTY,EMPTY),$2.a);
            }
        }
    }
    else{
        printf("Erreur : L'appel n'est pas fait sur une fonction ou procedure déclaré ligne %d\n",numligne);
        $$.a = creer_arbre_vide();
    }
  }
      ;

liste_arguments : PO liste_args PF {$$.a = $2.a;}
    ;

liste_args :  {$$.a = creer_arbre_vide();}
           | un_arg {$$.a = concat_pere_fils(creer_noeud(A_LISTE_ARGS,EMPTY,EMPTY),$1.a);}
           | liste_args V un_arg {$$.a = concat_fils_frere($1.a,concat_pere_fils(creer_noeud(A_LISTE_ARGS,EMPTY,EMPTY),$3.a));}
           ;

un_arg : expression {$$.a = $1.a;arg_fonction[nb_arg_fonction] = $1.type;nb_arg_fonction++;}
       ;

condition : SI eb ALORS liste_instructions SINON liste_instructions {$$.a = concat_pere_fils(creer_noeud(A_IF_THEN_ELSE,EMPTY,EMPTY),concat_fils_frere($2.a,concat_fils_frere($4.a,$6.a)));}
          ;

/* comme pour la condition il faut utiliser eb et non pas expression
tant_que : TANT_QUE expression FAIRE liste_instructions
         ;
*/

tant_que : TANT_QUE eb FAIRE liste_instructions {$$.a = concat_pere_fils(creer_noeud(A_WHILE,EMPTY,EMPTY),concat_fils_frere($2.a,$4.a));}
         ;

affectation : variable OPAFF expression {
    if($1.type != $3.type){printf("Erreur : Probleme d'affectation ligne %d\n",numligne);
        $$.a = creer_arbre_vide();
    }
    else{
        if ($1.type == T_ENTIER)
            $1.valeur.i = $3.valeur.i;
        else if ($1.type == T_REEL)
            $1.valeur.f = $3.valeur.f;
        else if ($1.type == T_CHAR)
            $1.valeur.c = $3.valeur.c;
        else
            $1.valeur.s = $3.valeur.s;
        $$.a = concat_pere_fils(creer_noeud(A_OPAFF,EMPTY,EMPTY),concat_fils_frere($1.a,$3.a));}
  }
            ;

variable : IDF suite_variable {
    printf("Dans variable\n");
    decl_associe=assoc_nom($1.valeur.i);
    if (decl_associe == -1){
        printf ("Erreur : Variable non déclaré ligne %d\n",numligne);
        $$.a = creer_arbre_vide();
    }
    else if (tab_declarations[decl_associe].nature != VAR){
        printf ("Erreur : Ce n'est pas une variable ligne %d\n",numligne);
        $$.a = creer_arbre_vide();
    }
    else{
        $$.type = tab_declarations[assoc_nom($1.valeur.i)].description;
        if(tab_declarations[$$.type].nature == TAB ) {
            $$.type = representations[tab_declarations[$$.type].description];
            verifier_dimensions(tab_declarations[assoc_nom($1.valeur.i)].description);
        }
        $$.a = concat_pere_fils(creer_noeud(A_IDF,$1.valeur.i,assoc_nom($1.valeur.i)),$2.a);}
  }
         ;

suite_variable : CO liste_indices CF var_struct {if (est_vide($4.a)){$$.a = concat_pere_fils(creer_noeud(A_TAB,EMPTY,EMPTY),$2.a);}else{$$.a = concat_pere_fils(creer_noeud(A_STRUCT,EMPTY,EMPTY),concat_fils_frere(creer_noeud(A_TAB,EMPTY,EMPTY),$4.a));};}
               | var_struct   {if (est_vide($1.a)){$$.a = creer_arbre_vide();}else{$$.a = concat_pere_fils(creer_noeud(A_STRUCT,EMPTY,EMPTY),$1.a);};}
	       ;

var_struct : P variable {$$.a = $2.a;}
| {printf("OK ??????\n");$$.a = creer_arbre_vide();}
           ;

liste_indices : ea {$$.a = concat_pere_fils(creer_noeud(A_LISTE_INDICE,EMPTY,EMPTY),$1.a); verif_dimensions[nb_dim_verif++] = $1.valeur.i;}
              | liste_indices V ea {$$.a = concat_pere_fils($1.a,concat_fils_frere(creer_noeud(A_LISTE_INDICE,EMPTY,EMPTY),$3.a));verif_dimensions[nb_dim_verif++] = $3.valeur.i;}
              ;


expression : ea {$$.type = $1.type; $$.valeur = $1.valeur; $$.a = $1.a;}
           | eb {$$.type = T_BOOL; $$.valeur = $1.valeur; $$.a = $1.a;}
           ;

eb : eb OU eb1   {printf("Dans le ||\n");$$.type = T_BOOL; $$.valeur.i = $1.valeur.i || $3.valeur.i; $$.a = concat_pere_fils(creer_noeud(A_OU,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
   | eb1         {$$.type = T_BOOL; $$.valeur.i = $1.valeur.i; $$.a = $1.a;}
   ;

eb1 : eb1 ET eb2 {printf("Dans le &&\n");$$.type = T_BOOL; $$.valeur.i = $1.valeur.i && $3.valeur.i; $$.a = concat_pere_fils(creer_noeud(A_ET,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
    | eb2        {$$.type = T_BOOL; $$.valeur.i = $1.valeur.i; $$.a = $1.a;}
    ;

eb2 : NON eb3 {$$.type = T_BOOL; if ($2.valeur.i == 0){ $$.valeur.i = 1; $$.a = creer_arbre_vide(); } else $$.valeur.i = 0; $$.a = concat_pere_fils(creer_noeud(A_NON,EMPTY,EMPTY), $2.a);}
    | eb3     {$$.type = T_BOOL; $$.valeur.i = $1.valeur.i; $$.a = $1.a;}
    ;

eb3 : PO eb PF     {$$.type = T_BOOL; $$.valeur.i = $2.valeur.i; $$.a = $2.a;}
    | VRAI         {$$.valeur.i = 1; $$.type = T_BOOL; $$.a = creer_noeud(A_VRAI,EMPTY,EMPTY);}
    | FAUX         {$$.valeur.i = 0; $$.type = T_BOOL; $$.a = creer_noeud(A_FAUX,EMPTY,EMPTY);}
    | comparaison  {$$.type = $1.type; $$.valeur.i = $1.valeur.i; $$.a = $1.a;}
    ;


comparaison : ea OPEG ea    {
    printf("Dans OPEG\n");
    if ($1.type != $3.type){
        printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
        $$.a = creer_arbre_vide();
    }
    else{
        $$.type = T_BOOL;
        if($1.type == T_ENTIER)
            $$.valeur.i = $1.valeur.i == $3.valeur.i;
        else if($1.type == T_REEL) $$.valeur.i = $1.valeur.f == $3.valeur.f;
        else if($1.type == T_CHAR) $$.valeur.i = $1.valeur.c == $3.valeur.c;
        else printf("Erreur : Comparaison impossible ligne %d",numligne);
        $$.a = concat_pere_fils(creer_noeud(A_OPEG,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}
  }


            | ea OPDIF ea   {
                if ($1.type != $3.type){
                    printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
                    $$.a = creer_arbre_vide();
                }
                else{
                    $$.type = T_BOOL;
                    if($1.type == T_ENTIER)
                        $$.valeur.i = $1.valeur.i == $3.valeur.i;
                    else if($1.type == T_REEL) $$.valeur.i = $1.valeur.f == $3.valeur.f;
                    else if($1.type == T_CHAR) $$.valeur.i = $1.valeur.c == $3.valeur.c; 
                    else printf("Erreur : Comparaison impossible ligne %d",numligne);
                    $$.a = concat_pere_fils(creer_noeud(A_OPDIF,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}}


            | ea OPINF ea   {
                if ($1.type != $3.type){
                    printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
                    $$.a = creer_arbre_vide();
                }
                else{
                    $$.type = T_BOOL;
                    if($1.type == T_ENTIER)
                        $$.valeur.i = $1.valeur.i == $3.valeur.i;
                    else if($1.type == T_REEL) $$.valeur.i = $1.valeur.f == $3.valeur.f;
                    else if($1.type == T_CHAR) $$.valeur.i = $1.valeur.c == $3.valeur.c; 
                    else printf("Erreur :Comparaison impossible ligne %d",numligne);   
                    $$.a = concat_pere_fils(creer_noeud(A_OPINF,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}}


            | ea OPSUP ea   {
                if ($1.type != $3.type){
                    printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
                    $$.a = creer_arbre_vide();
                }
                else{
                    $$.type = T_BOOL;
                    if($1.type == T_ENTIER)
                        $$.valeur.i = $1.valeur.i == $3.valeur.i;
                    else if($1.type == T_REEL) $$.valeur.i = $1.valeur.f == $3.valeur.f;
                    else if($1.type == T_CHAR) $$.valeur.i = $1.valeur.c == $3.valeur.c;
                    else printf("Erreur : Comparaison impossible ligne %d",numligne);
                    $$.a = concat_pere_fils(creer_noeud(A_OPSUP,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}}


            | ea OPINFEG ea {
                if ($1.type != $3.type){
                    printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
                    $$.a = creer_arbre_vide();
                }
                else{
                    $$.type = T_BOOL;
                    if($1.type == T_ENTIER)
                        $$.valeur.i = $1.valeur.i == $3.valeur.i;
                    else if($1.type == T_REEL) $$.valeur.i = $1.valeur.f == $3.valeur.f;
                    else if($1.type == T_CHAR) $$.valeur.i = $1.valeur.c == $3.valeur.c;
                    else printf("Erreur : Comparaison impossible ligne %d",numligne);
                    $$.a = concat_pere_fils(creer_noeud(A_OPINFEG,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}}


            | ea OPSUPEG ea {
                if ($1.type != $3.type){
                    printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
                    $$.a = creer_arbre_vide();
                }
                else{$$.type = T_BOOL;
                    if($1.type == T_ENTIER)
                        $$.valeur.i = $1.valeur.i == $3.valeur.i;
                    else if($1.type == T_REEL) $$.valeur.i = $1.valeur.f == $3.valeur.f;
                    else if($1.type == T_CHAR) $$.valeur.i = $1.valeur.c == $3.valeur.c;
                    else printf("Comparaison impossible ligne %d",numligne);
                    $$.a = concat_pere_fils(creer_noeud(A_OPSUPEG,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}}
;


ea : ea PLUS ea1 {
    if ($1.type != $3.type){
        printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
        $$.a = creer_arbre_vide();
    }
      else{$$.type = $1.type; 
          if($1.type == T_ENTIER)
              $$.valeur.i = $1.valeur.i + $3.valeur.i;
          else if($1.type == T_REEL)
              $$.valeur.f = $1.valeur.f + $3.valeur.f;
          else if($1.type == T_CHAR)
              $$.valeur.c = $1.valeur.c + $3.valeur.c;
          else if($1.type == T_STRING)
              $$.valeur.s = strcat($1.valeur.s,$3.valeur.s);
          else
              printf("Erreur : Addition impossible pour ce type ligne %d\n",numligne);
          $$.a = concat_pere_fils(creer_noeud(A_PLUS,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}}


   | ea MOINS ea1 {
       if ($1.type != $3.type){
           printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
           $$.a = creer_arbre_vide();
       }
      else{$$.type = $1.type;
          if($1.type == T_ENTIER)
              $$.valeur.i = $1.valeur.i - $3.valeur.i;
          else if($1.type == T_REEL)
              $$.valeur.f = $1.valeur.f - $3.valeur.f;
          else
              printf("Erreur : Soustraction impossible pour ce type ligne %d \n",numligne);
          $$.a = concat_pere_fils(creer_noeud(A_MOINS,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}}


   | ea1 {$$.type = $1.type; $$.valeur = $1.valeur; $$.a = $1.a;}
   ;

ea1 : ea1 MULT ea2 {
    if ($1.type != $3.type){
        printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
        $$.a = creer_arbre_vide();
    }
    else{$$.type = $1.type;
        if($1.type == T_ENTIER)
            $$.valeur.i = $1.valeur.i * $3.valeur.i;
        else if($1.type == T_REEL)
            $$.valeur.f = $1.valeur.f * $3.valeur.f;
        else
            printf("Erreur : Multiplication impossible pour ce type ligne %d \n",numligne);
        $$.a = concat_pere_fils(creer_noeud(A_MULT,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}}



    | ea1 DIV ea2 {
        if ($1.type != $3.type){
            printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
            $$.a = creer_arbre_vide();
        }
        else{$$.type = $1.type;
            if($1.type == T_ENTIER){
                if ($3.valeur.i == 0) printf("Erreur : Division par 0 impossible ligne %d\n",numligne); 
                $$.valeur.i = $1.valeur.i / $3.valeur.i;
            }
            else if($1.type == T_REEL){
                if ($3.valeur.f == 0.0) printf("Erreur : Division par 0 impossible ligne %d\n",numligne); 
                $$.valeur.f = $1.valeur.f / $3.valeur.f;
            }
            else
                printf("Erreur : Division impossible pour ce type ligne %d \n",numligne);
            $$.a = concat_pere_fils(creer_noeud(A_DIV,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}}


    | ea1 MOD ea2 {
        if ($1.type != $3.type){
            printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
            $$.a = creer_arbre_vide();
        }
        else{$$.type = $1.type;
            if($1.type == T_ENTIER)
                $$.valeur.i = $1.valeur.i % $3.valeur.i;
            else
                printf("Erreur : Modulo impossible pour ce type ligne %d \n",numligne);
            $$.a = concat_pere_fils(creer_noeud(A_MOD,EMPTY,EMPTY), concat_fils_frere($1.a,$3.a));}}


    | ea2 {$$.valeur = $1.valeur; $$.type = $1.type; $$.a = $1.a;}
    ;

ea2 : PO ea PF       {$$.valeur = $2.valeur; $$.type = $2.type; $$.a = $2.a;}
    | CSTE_ENTIERE   {$$.valeur.i = $1.valeur.i; $$.type = T_ENTIER; $$.a = creer_noeud(A_INT, EMPTY, EMPTY);}
    | CSTE_REELE     {$$.valeur.f = $1.valeur.f; $$.type = T_REEL;$$.a =  creer_noeud(A_FLOAT,EMPTY, EMPTY);}
    | CHAINE         {$$.valeur.s = $1.valeur.s; $$.type = T_STRING; $$.a = creer_noeud(A_CHAINE,EMPTY,EMPTY);}
    | UN_CARACTERE   {$$.valeur.c = $1.valeur.c; $$.type = T_CHAR;$$.a = creer_noeud(A_CHAR,EMPTY,EMPTY);}
    | variable       {$$.type = $1.type; if ($$.type == T_ENTIER) $$.valeur.i = $1.valeur.i; else if($$.type == T_REEL)$$.valeur.f = $1.valeur.f; else if($$.type == T_CHAR)$$.valeur.c = $1.valeur.c; else $$.valeur.s = $1.valeur.s; $$.a = $1.a;}
    | appel          {$$.type = $1.type; if ($$.type == T_ENTIER) $$.valeur.i = $1.valeur.i; else if($$.type == T_REEL)$$.valeur.f = $1.valeur.f; else if($$.type == T_CHAR)$$.valeur.c = $1.valeur.c; else $$.valeur.s = $1.valeur.s; $$.a = $1.a;memset(arg_fonction,-1,sizeof(int)*MAX_ARG);nb_arg_fonction;}
    ;


%%
int yyerror(){
printf("Erreur de syntaxe en ligne %d\n", numligne);
return -1 ;
}

int verife_arg_fonction(int num_lexico){
    int i;
    int type = tab_declarations[assoc_nom(num_lexico)].description + 1;
    int res = 0;
    /*verife nombre d'arguments */
    if(nb_arg_fonction != representations[type]){
        printf("Erreur : Le nombre d'argument de la fonction %s en ligne %d est different de la declaration\n",tb_lex[num_lexico].lexem, numligne);
        res = -1;
    }
    /* verifie le type des arguments */
    else {
        for(i=0; i<nb_arg_fonction; i++){
            type += 2;
            if(arg_fonction[i] != representations[type]){
                printf("Erreur : L'argument %d de la fonction %s en ligne %d est different de la declaration\n",i+1,tb_lex[num_lexico].lexem, numligne);
                res = -1;
            }
        }
    }
    return res;
}

int verifier_dimensions (int num_dec_type){
    int i;
    int res;
    int verife = (tab_declarations[num_dec_type].description) + 1;
/*verifie nombre de dimensions*/
    if(nb_dim_verif != representations[verife]){
        printf("Erreur : Le nombre %d de dimensions est incorect ligne %d\n",nb_dim_verif,numligne);
        res -1;
    }
    else{
        /*verifie la valeur de la dimension*/
        for(i = 0; i < nb_dim_verif; i++){
            verife ++;
            if (verif_dimensions[i] < representations[verife]){
                printf("Erreur : La valeur de la dimension %d est inccorect\n",i);
                res = -1;
            }
            verife ++;
            if (verif_dimensions[i] > representations[verife]){
                printf("Erreur : La valeur de la dimension %d est inccorect\n",i);
                res = 0;
            }
        } 
    }
    return res;
}

int main(){
yyparse() ;
affichage_lexeme();
affiche_declarations();
affiche_representation();
afficher_region();
}
