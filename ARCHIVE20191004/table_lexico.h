#ifndef _table_lexico_h_
#define _table_lexico_h_

typedef struct table_lexico{
    int longueur;
    char *lexem;
    int suivant;
} table_lexico;

typedef struct table_hashcode{
    int hashcode;
    int position;
}table_hashcode;

table_lexico tb_lex[500];
table_hashcode tb_hash[40];

int ajouter_lexeme(char *lex);
void init_table_lex();
void affichage_lexeme();

#endif