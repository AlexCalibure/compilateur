#include <stdlib.h>
#include <stdio.h>

#define TAILLE 510
#define LIMITE 200
#define VIDE -1
#define BASE 0
#define STRUCT 1
#define TAB 2
#define VAR 3
#define PARAM 4
#define PROC 5
#define FCT 6

typedef struct declaration {
    int nature;
    int suivant;
    int region;
    int description;
    //int execution;
}declarations;


void init_declarations (declarations tab_declarations[]);

void ajouter_declaration1 (declarations tab_declarations[], int indice, type_nature nature, int region, int description/*, int execution*/ );