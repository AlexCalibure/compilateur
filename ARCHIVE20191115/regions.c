#include"regions.h"
#include<string.h>

int indice_pile; //Sert à connaitre le sommet de la pile

//Fonctions concernant la table des regions

void initialiser_region(){
    int i;
    for(i=0; i<TAILLEMAX; i++){
        tab_region[i].taille = -1;
        tab_region[i].NIS = -1;
        tab_region[i].arbre = -1;
    }
}



void ajouter_region(int indice, int taille, int NIS , int arbre){
    tab_region[indice].taille = taille;
    tab_region[indice].NIS = NIS;
    tab_region[indice].arbre = arbre;
}

void afficher_region(){
    int i;
    printf("\nTable des regions \n");
    printf(" Taille |  NIS  |  Arbre\n");
    for(i=0;i<TAILLEMAX;i++){
        printf("   %d   |  %d   |  %d\n", tab_region[i].taille, tab_region[i].NIS, tab_region[i].arbre);
    }
    printf("\n");
           
}

//Fonctions concernant les piles de region

void init_pile(){
    int i;
    pile[0] = 0;
    indice_pile = pile[0]+1;
    for(i=1;i<TAILLEMAX;i++)
        pile[i] = -1;
}

void empiler(){
    pile[indice_pile] = indice_pile;
    indice_pile++;
}


void depiler(){
    pile[indice_pile] = -1;
    indice_pile --;
}

int sommet(){
    return pile[indice_pile];
}

void affiche_pile(){
    int i;
    printf("\nPile des regions\n");
    for(i=0;i<indice_pile;i++){
        printf(" %d | ", pile[i]); 
    }
    printf("\n");
}