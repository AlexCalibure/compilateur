#include"regions.h"
#include<string.h>

//Fonctions concernant la table des regions

void initialiser_region(){
    int i;
    for(i=0; i<TAILLE_MAX; i++){
        tab_region[i].taille = -1;
        tab_region[i].NIS = -1;
        tab_region[i].arbre = -1;
    }
}

void ajouter_region(int indice,int nis){
    tab_region[indice].taille = 0;
    tab_region[indice].NIS = nis;
    tab_region[indice].arbre = 0;
}

void afficher_region(){
    int i;
    printf("\nTable des regions \n");
    printf(" Taille |  NIS  |  Arbre\n");
    for(i=0;i<TAILLE_MAX;i++){
        printf("   %d   |  %d   |  %d\n", tab_region[i].taille, tab_region[i].NIS, tab_region[i].arbre);
    }
    printf("\n");
           
}

void modifier_taille_region(int indice, int taille){
    tab_region[indice].taille += taille;
}



//Fonctions concernant les piles de region

int pile_vide(pile p1){
    return (p1.t_pile[0] == -1);
}

pile init_pile(pile p1){
    int i;
    p1.sommet_pile = -1;
    for(i=0;i<TAILLE_MAX;i++)
        p1.t_pile[i] = -1;
    return p1;
}

pile empiler(pile p1, int region){
    p1.sommet_pile++;
    p1.t_pile[p1.sommet_pile] = region;
    return p1;
}


pile depiler(pile p1){
    p1.t_pile[p1.sommet_pile] = -1;
    p1.sommet_pile --;
    return p1;
}

int sommet(pile p1){
    return p1.t_pile[p1.sommet_pile];
}

int taille(pile p1){
    return p1.sommet_pile;
}
pile affiche_pile(pile p1){
    int i;
    printf("\nPile des regions\n");
    printf("Sommet de pile %d\n",p1.sommet_pile);
    for(i=0;i<=p1.sommet_pile;i++){
        printf(" %d | ", p1.t_pile[i]); 
    }
    printf("\n");
    return p1;
}