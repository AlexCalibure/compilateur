#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "table_lexico.h"

int indice_tab_lex = 0;
int indice_tab_hashcode = 0;

int num_hashcode(char *lex)
{
    int i;
    int hashcode = 0;
    for(i=0;i<strlen(lex);i++){
        hashcode += lex[i];
    }
    return hashcode%32;
}

int chercher_hashcode(char *lex)
{
    int i=0, place;
    int res;
    int hashcode = num_hashcode(lex);
    // recherche si hashcode present
    while((i<indice_tab_hashcode)){
        if(tb_hash[i].hashcode == hashcode){
            place = tb_hash[i].position;
            return place;
        }
        i++;
    }
    
    // si absent ajout a la table
    res = indice_tab_hashcode;
    tb_hash[res].hashcode = hashcode;
    tb_hash[res].position = indice_tab_lex;
    indice_tab_hashcode++;
        //}
    return res;
}

int ajouter_lexeme(char *lex)
{
    int place;
    int hashcode = num_hashcode(lex);
    // recherche si hashcode present et l'ajoute sinon
    place = chercher_hashcode(lex);
    // si hashcode absent ajout du lexeme dans la table lexico
    if(place == indice_tab_lex){
        tb_lex[indice_tab_lex].longueur = hashcode;
        tb_lex[indice_tab_lex].lexem = lex;
        printf("Ajout de %s dans table lexico\n",lex);
        tb_lex[indice_tab_lex].suivant = -1;
        indice_tab_lex++;
    }
    //si hashcode present recherche lexeme similaire
    else{
        printf("Hashcode de %s present dans le lexem\n",lex);
        while(place != -1){
            if(strcmp(tb_lex[place].lexem,lex) == 0){
                printf("deja dans le tab lex place %d\n",place);
                return place;
            }
            else
                place = tb_lex[place].suivant;
        } 

        // si aucun similaire ajout a la fin
        tb_lex[indice_tab_lex].longueur = hashcode;
        tb_lex[indice_tab_lex].lexem = lex;
        tb_lex[indice_tab_lex].suivant = -1;
        indice_tab_lex++;
    }

    return indice_tab_lex-1;
}

// initialise le tableau lexicographique a l'arriver de programme
void init_table_lex()
{
    // int
    tb_lex[0].longueur = ('i'+'n'+'t')%32;
    tb_lex[0].lexem = "int";
    tb_lex[0].suivant = -1;
    tb_hash[0].hashcode = tb_lex[0].longueur;
    tb_hash[0].position = 0;
    
    // float
    tb_lex[1].longueur = ('f'+'l'+'o'+'a'+'t')%32;
    tb_lex[1].lexem = "float";
    tb_lex[1].suivant = -1;
    tb_hash[1].hashcode = tb_lex[1].longueur;
    tb_hash[1].position = 1;
    
    // char
    tb_lex[2].longueur = ('c'+'h'+'a'+'r')%32;
    tb_lex[2].lexem = "char";
    tb_lex[2].suivant = -1;
    tb_hash[2].hashcode = tb_lex[2].longueur;
    tb_hash[2].position = 2;
    
    //bool
    tb_lex[3].longueur = ('b'+'o'+'o'+'l')%32;
    tb_lex[3].lexem = "bool";
    tb_lex[3].suivant = -1;
    tb_hash[3].hashcode = tb_lex[3].longueur;
    tb_hash[3].position = 3;
    
    indice_tab_lex = 4;
    indice_tab_hashcode = 4;
}

void affichage_lexeme()
{
    int i;

    printf("\nAffichage tableau hashcode\n");
    printf("  Hashcode    |    Position\n");
    for(i=0;i<indice_tab_lex;i++){
        printf("  %d    |    %d\n",tb_hash[i].hashcode,tb_hash[i].position);
    }
    printf("\n");
    printf("Affichage tableau lexeme\n");
    printf("  long    |    lexeme    |    suivant\n");
    for(i=0;i<indice_tab_lex;i++){
        printf("  %d    |    %s    |    %d\n",tb_lex[i].longueur,tb_lex[i].lexem,tb_lex[i].suivant);
    }
}
