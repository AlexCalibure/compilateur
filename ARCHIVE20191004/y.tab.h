/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    PROG = 258,
    PV = 259,
    DEBUT = 260,
    FIN = 261,
    TYPE = 262,
    IDF = 263,
    DP = 264,
    STRUCT = 265,
    FSTRUCT = 266,
    TABLEAU = 267,
    FLECHE = 268,
    ENTIER = 269,
    REEL = 270,
    BOOLEEN = 271,
    CARACTERE = 272,
    CHAINE = 273,
    CSTE_ENTIERE = 274,
    CSTE_REELE = 275,
    RETOURNE = 276,
    PO = 277,
    PF = 278,
    VIDE = 279,
    SI = 280,
    ALORS = 281,
    SINON = 282,
    TANT_QUE = 283,
    FAIRE = 284,
    OPAFF = 285,
    CO = 286,
    CF = 287,
    P = 288,
    V = 289,
    OU = 290,
    ET = 291,
    NON = 292,
    VRAI = 293,
    FAUX = 294,
    OPEG = 295,
    OPDIF = 296,
    OPINF = 297,
    OPSUP = 298,
    OPINFEG = 299,
    OPSUPEG = 300,
    PLUS = 301,
    MOINS = 302,
    MULT = 303,
    DIV = 304,
    MOD = 305,
    NEG = 306,
    VARIABLE = 307,
    PROCEDURE = 308,
    FONCTION = 309,
    UN_CARACTERE = 310,
    P_P = 311
  };
#endif
/* Tokens.  */
#define PROG 258
#define PV 259
#define DEBUT 260
#define FIN 261
#define TYPE 262
#define IDF 263
#define DP 264
#define STRUCT 265
#define FSTRUCT 266
#define TABLEAU 267
#define FLECHE 268
#define ENTIER 269
#define REEL 270
#define BOOLEEN 271
#define CARACTERE 272
#define CHAINE 273
#define CSTE_ENTIERE 274
#define CSTE_REELE 275
#define RETOURNE 276
#define PO 277
#define PF 278
#define VIDE 279
#define SI 280
#define ALORS 281
#define SINON 282
#define TANT_QUE 283
#define FAIRE 284
#define OPAFF 285
#define CO 286
#define CF 287
#define P 288
#define V 289
#define OU 290
#define ET 291
#define NON 292
#define VRAI 293
#define FAUX 294
#define OPEG 295
#define OPDIF 296
#define OPINF 297
#define OPSUP 298
#define OPINFEG 299
#define OPSUPEG 300
#define PLUS 301
#define MOINS 302
#define MULT 303
#define DIV 304
#define MOD 305
#define NEG 306
#define VARIABLE 307
#define PROCEDURE 308
#define FONCTION 309
#define UN_CARACTERE 310
#define P_P 311

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
