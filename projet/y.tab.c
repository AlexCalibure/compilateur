/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "cpyrr.y" /* yacc.c:339  */

    #include <stdio.h>
     #include<string.h>
    #include "declarations.h"
    #include "representation.h"
    #include "arbre_abstrait.h"
    #include "table_lexico.h"

     #define T_ENTIER 0
     #define T_REEL 1
     #define T_CHAR 2
     #define T_STRING -3
     #define T_BOOL 3


     #define MAX_ARG 50
     #define MAX_DIM 50
     
     extern int yyerror();
     extern int yylex();
     int verife_arg_fonction(int numelexico);
     int verifier_dimensions(int num_dec_type);
     extern int numligne;
     extern int region;
     extern int nbr_champs;
     extern int nbr_dimensions;
     extern int nbr_params;
     extern int champs[];
     extern int dimensions[];
     extern int params[];
     extern int indice;
     int test_assoc;
     int nb_arg_fonction = 0;
     int arg_fonction[MAX_ARG];
     int verif_dimensions[MAX_DIM];
     int nb_dim_verif = 0;
     int decl_associe;

#line 105 "y.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    PROG = 258,
    PV = 259,
    DEBUT = 260,
    FIN = 261,
    TYPE = 262,
    DP = 263,
    STRUCT = 264,
    FSTRUCT = 265,
    TABLEAU = 266,
    FLECHE = 267,
    ENTIER = 268,
    REEL = 269,
    BOOLEEN = 270,
    CARACTERE = 271,
    RETOURNE = 272,
    PO = 273,
    PF = 274,
    VIDE = 275,
    SI = 276,
    ALORS = 277,
    SINON = 278,
    TANT_QUE = 279,
    FAIRE = 280,
    OPAFF = 281,
    CO = 282,
    CF = 283,
    P = 284,
    V = 285,
    OU = 286,
    ET = 287,
    NON = 288,
    VRAI = 289,
    FAUX = 290,
    OPEG = 291,
    OPDIF = 292,
    OPINF = 293,
    OPSUP = 294,
    OPINFEG = 295,
    OPSUPEG = 296,
    PLUS = 297,
    MOINS = 298,
    MULT = 299,
    DIV = 300,
    MOD = 301,
    NEG = 302,
    VARIABLE = 303,
    PROCEDURE = 304,
    FONCTION = 305,
    BEG = 306,
    END = 307,
    P_P = 308,
    IDF = 309,
    CSTE_ENTIERE = 310,
    CSTE_REELE = 311,
    CHAINE = 312,
    UN_CARACTERE = 313
  };
#endif
/* Tokens.  */
#define PROG 258
#define PV 259
#define DEBUT 260
#define FIN 261
#define TYPE 262
#define DP 263
#define STRUCT 264
#define FSTRUCT 265
#define TABLEAU 266
#define FLECHE 267
#define ENTIER 268
#define REEL 269
#define BOOLEEN 270
#define CARACTERE 271
#define RETOURNE 272
#define PO 273
#define PF 274
#define VIDE 275
#define SI 276
#define ALORS 277
#define SINON 278
#define TANT_QUE 279
#define FAIRE 280
#define OPAFF 281
#define CO 282
#define CF 283
#define P 284
#define V 285
#define OU 286
#define ET 287
#define NON 288
#define VRAI 289
#define FAUX 290
#define OPEG 291
#define OPDIF 292
#define OPINF 293
#define OPSUP 294
#define OPINFEG 295
#define OPSUPEG 296
#define PLUS 297
#define MOINS 298
#define MULT 299
#define DIV 300
#define MOD 301
#define NEG 302
#define VARIABLE 303
#define PROCEDURE 304
#define FONCTION 305
#define BEG 306
#define END 307
#define P_P 308
#define IDF 309
#define CSTE_ENTIERE 310
#define CSTE_REELE 311
#define CHAINE 312
#define UN_CARACTERE 313

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 40 "cpyrr.y" /* yacc.c:355  */

    struct{
        arbre a;
        int type;
        union{
            int i;
            char c;
            float f;
            char *s;
        }valeur;
    }struct_arbre;
    struct{
        int type;
        union{
            int i;
            char c;
            float f;
            char *s;
        }valeur;
    }struct_simple;

#line 283 "y.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 300 "y.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   201

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  59
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  46
/* YYNRULES -- Number of rules.  */
#define YYNRULES  96
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  181

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   313

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    72,    72,    75,    78,    81,    82,    85,    86,    89,
      90,    93,    94,    97,    98,    99,   103,   115,   116,   119,
     122,   123,   133,   136,   137,   140,   143,   144,   147,   148,
     149,   150,   151,   154,   157,   160,   163,   164,   167,   168,
     171,   174,   175,   176,   177,   178,   179,   183,   184,   193,
     213,   216,   217,   218,   221,   224,   232,   235,   252,   273,
     274,   277,   278,   281,   282,   286,   287,   290,   291,   294,
     295,   298,   299,   302,   303,   304,   305,   309,   326,   341,
     356,   371,   386,   401,   420,   435,   438,   454,   473,   486,
     489,   490,   491,   492,   493,   494,   495
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "PROG", "PV", "DEBUT", "FIN", "TYPE",
  "DP", "STRUCT", "FSTRUCT", "TABLEAU", "FLECHE", "ENTIER", "REEL",
  "BOOLEEN", "CARACTERE", "RETOURNE", "PO", "PF", "VIDE", "SI", "ALORS",
  "SINON", "TANT_QUE", "FAIRE", "OPAFF", "CO", "CF", "P", "V", "OU", "ET",
  "NON", "VRAI", "FAUX", "OPEG", "OPDIF", "OPINF", "OPSUP", "OPINFEG",
  "OPSUPEG", "PLUS", "MOINS", "MULT", "DIV", "MOD", "NEG", "VARIABLE",
  "PROCEDURE", "FONCTION", "BEG", "END", "P_P", "IDF", "CSTE_ENTIERE",
  "CSTE_REELE", "CHAINE", "UN_CARACTERE", "$accept", "programme", "corps",
  "liste_declarations", "liste_declarations_type",
  "liste_declarations_variable", "liste_declarations_pf",
  "liste_instructions", "declaration_pf", "declaration_type",
  "suite_declaration_type", "dimension", "liste_dimensions",
  "une_dimension", "liste_champs", "un_champ", "nom_type", "type_simple",
  "declaration_variable", "declaration_procedure", "declaration_fonction",
  "liste_parametres", "liste_param", "un_param", "instruction",
  "resultat_retourne", "appel", "liste_arguments", "liste_args", "un_arg",
  "condition", "tant_que", "affectation", "variable", "suite_variable",
  "var_struct", "liste_indices", "expression", "eb", "eb1", "eb2", "eb3",
  "comparaison", "ea", "ea1", "ea2", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313
};
# endif

#define YYPACT_NINF -139

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-139)))

#define YYTABLE_NINF -16

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      11,    21,    26,  -139,  -139,    23,    20,    82,  -139,    36,
    -139,    36,    36,    89,    20,   106,  -139,  -139,  -139,  -139,
      96,    69,   117,   128,    36,    47,  -139,  -139,  -139,  -139,
    -139,  -139,  -139,  -139,  -139,  -139,   115,   135,  -139,  -139,
    -139,   120,    85,  -139,    14,   120,    88,    36,    29,   114,
    -139,  -139,  -139,   165,  -139,    36,   162,   118,     0,   167,
    -139,   -12,    98,  -139,    36,    36,    29,    29,    29,    29,
      29,    29,    29,    29,    29,    29,    29,    20,    20,    48,
    -139,  -139,    29,   119,    55,   123,  -139,  -139,  -139,   142,
     166,   121,   122,   169,  -139,  -139,  -139,  -139,  -139,   135,
    -139,    55,    55,    55,    55,    55,    55,    85,    85,  -139,
    -139,  -139,    10,    20,  -139,    36,    13,   148,    29,   124,
     152,  -139,     9,   163,   163,  -139,    20,  -139,  -139,    55,
     172,   116,  -139,    29,   170,  -139,  -139,  -139,  -139,  -139,
     156,  -139,  -139,   130,   180,   171,    20,     9,   124,  -139,
     136,  -139,   -25,     9,   131,   179,    16,  -139,  -139,    -4,
    -139,  -139,  -139,    29,    29,  -139,   161,    -4,   130,  -139,
     184,   186,  -139,    55,  -139,  -139,  -139,  -139,  -139,   187,
    -139
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,     6,     1,     0,     0,     8,     2,    47,
      45,     0,     0,    62,     3,     0,    44,    42,    43,    41,
       0,     0,    10,     0,     0,     0,    74,    75,    91,    92,
      93,    94,    46,    96,    95,    48,    66,    68,    70,    72,
      76,    65,    85,    89,     0,     0,     0,    51,     0,     0,
      49,    58,    60,     0,    11,     0,     0,     0,     4,     0,
       5,     0,     0,    71,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      52,    54,     0,     0,    63,    62,    61,    12,    57,     0,
       0,     0,     0,     0,    14,    13,     7,    73,    90,    67,
      69,    77,    78,    79,    80,    81,    82,    83,    84,    86,
      87,    88,     0,    56,    50,     0,     0,    62,     0,     0,
       0,    16,     0,    36,    36,     9,     0,    53,    59,    64,
       0,     0,    23,     0,     0,    28,    29,    30,    31,    27,
       0,    33,    26,     0,     0,     0,    55,     0,     0,    17,
       0,    20,     0,     0,     0,     0,     0,    38,     6,     0,
      25,    24,    19,     0,     0,    18,     0,     0,     0,    37,
       0,     0,    21,    22,    32,    40,    39,    34,     6,     0,
      35
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -139,  -139,   -90,  -139,  -139,  -139,  -139,   -75,  -139,  -139,
    -139,  -139,  -139,    31,  -139,    44,   -26,  -138,  -139,  -139,
    -139,    71,  -139,    28,   -13,  -139,     2,  -139,  -139,    83,
    -139,  -139,  -139,    -1,  -139,    80,  -139,    -3,    84,   137,
     134,   175,  -139,    -9,   -30,    68
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     5,     6,     7,    22,    58,    14,    93,    23,
     121,   134,   150,   151,   131,   132,   141,   142,    59,    94,
      95,   144,   156,   157,    15,    32,    33,    50,    79,    80,
      17,    18,    19,    34,    51,    52,    83,    81,    36,    37,
      38,    39,    40,    45,    42,    43
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      41,    53,   112,   113,   -15,    20,    35,    97,    16,   135,
     136,   137,   138,    20,     1,    62,    16,    72,    73,    64,
     168,   171,   135,   136,   137,   138,     4,     9,   164,   175,
      10,    11,    98,   126,    12,   169,    77,     9,    41,    84,
      10,    11,   107,   108,    12,    64,    41,    82,    86,    91,
      92,   146,    88,   140,    24,    72,    73,   101,   102,   103,
     104,   105,   106,   139,    13,    24,   140,   114,   170,    25,
      26,    27,     3,   116,    13,     8,    20,    20,   115,    16,
      16,    26,    27,    13,    28,    29,    30,    31,   179,    21,
      13,    28,    29,    30,    31,    44,    46,    72,    73,    53,
      53,    13,    28,    29,    30,    31,    41,    47,    61,   129,
      54,    20,    20,    78,    16,    16,    48,    98,    49,    64,
     148,   160,    55,    56,   152,    20,   149,   165,    16,    74,
      75,    76,    60,    53,    66,    67,    68,    69,    70,    71,
      72,    73,   109,   110,   111,    20,    64,   117,    16,   118,
      48,   119,    49,   120,   152,   173,    66,    67,    68,    69,
      70,    71,    72,    73,   162,    57,   163,    65,    85,    87,
      89,    96,    90,   125,   122,   123,   124,    49,   130,   133,
     147,   143,   153,   154,   155,   158,   166,   167,   159,   174,
     177,   178,   161,   180,   172,   145,   176,   128,   127,   100,
      63,    99
};

static const yytype_uint8 yycheck[] =
{
       9,    14,    77,    78,     4,     6,     9,    19,     6,    13,
      14,    15,    16,    14,     3,    24,    14,    42,    43,    31,
       4,   159,    13,    14,    15,    16,     0,    17,    53,   167,
      20,    21,    19,    23,    24,    19,    22,    17,    47,    48,
      20,    21,    72,    73,    24,    31,    55,    18,    49,    49,
      50,   126,    55,    57,    18,    42,    43,    66,    67,    68,
      69,    70,    71,    54,    54,    18,    57,    19,   158,    33,
      34,    35,    51,    82,    54,    52,    77,    78,    30,    77,
      78,    34,    35,    54,    55,    56,    57,    58,   178,     7,
      54,    55,    56,    57,    58,    11,    12,    42,    43,   112,
     113,    54,    55,    56,    57,    58,   115,    18,    24,   118,
       4,   112,   113,    25,   112,   113,    27,    19,    29,    31,
       4,   147,    26,    54,   133,   126,    10,   153,   126,    44,
      45,    46,     4,   146,    36,    37,    38,    39,    40,    41,
      42,    43,    74,    75,    76,   146,    31,    28,   146,    30,
      27,     9,    29,    11,   163,   164,    36,    37,    38,    39,
      40,    41,    42,    43,    28,    48,    30,    32,    54,     4,
       8,     4,    54,     4,     8,    54,    54,    29,    54,    27,
       8,    18,    12,    27,    54,     5,    55,     8,    17,    28,
       6,     5,   148,     6,   163,   124,   168,   117,   115,    65,
      25,    64
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,    60,    51,     0,    61,    62,    63,    52,    17,
      20,    21,    24,    54,    66,    83,    85,    89,    90,    91,
      92,     7,    64,    68,    18,    33,    34,    35,    55,    56,
      57,    58,    84,    85,    92,    96,    97,    98,    99,   100,
     101,   102,   103,   104,    97,   102,    97,    18,    27,    29,
      86,    93,    94,    83,     4,    26,    54,    48,    65,    77,
       4,    97,   102,   100,    31,    32,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    22,    25,    87,
      88,    96,    18,    95,   102,    54,    92,     4,    96,     8,
      54,    49,    50,    67,    78,    79,     4,    19,    19,    98,
      99,   102,   102,   102,   102,   102,   102,   103,   103,   104,
     104,   104,    66,    66,    19,    30,   102,    28,    30,     9,
      11,    69,     8,    54,    54,     4,    23,    88,    94,   102,
      54,    73,    74,    27,    70,    13,    14,    15,    16,    54,
      57,    75,    76,    18,    80,    80,    66,     8,     4,    10,
      71,    72,   102,    12,    27,    54,    81,    82,     5,    17,
      75,    74,    28,    30,    53,    75,    55,     8,     4,    19,
      61,    76,    72,   102,    28,    76,    82,     6,     5,    61,
       6
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    59,    60,    61,    62,    63,    63,    64,    64,    65,
      65,    66,    66,    67,    67,    67,    68,    69,    69,    70,
      71,    71,    72,    73,    73,    74,    75,    75,    76,    76,
      76,    76,    76,    77,    78,    79,    80,    80,    81,    81,
      82,    83,    83,    83,    83,    83,    83,    84,    84,    85,
      86,    87,    87,    87,    88,    89,    90,    91,    92,    93,
      93,    94,    94,    95,    95,    96,    96,    97,    97,    98,
      98,    99,    99,   100,   100,   100,   100,   101,   101,   101,
     101,   101,   101,   102,   102,   102,   103,   103,   103,   103,
     104,   104,   104,   104,   104,   104,   104
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     4,     2,     3,     3,     0,     3,     0,     3,
       0,     2,     3,     1,     1,     0,     4,     3,     4,     3,
       1,     3,     3,     1,     3,     3,     1,     1,     1,     1,
       1,     1,     4,     4,     6,     8,     0,     3,     1,     3,
       3,     1,     1,     1,     1,     1,     2,     0,     1,     2,
       3,     0,     1,     3,     1,     6,     4,     3,     2,     4,
       1,     2,     0,     1,     3,     1,     1,     3,     1,     3,
       1,     2,     1,     3,     1,     1,     1,     3,     3,     3,
       3,     3,     3,     3,     3,     1,     3,     3,     3,     1,
       3,     1,     1,     1,     1,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 3:
#line 75 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 1522 "y.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 82 "cpyrr.y" /* yacc.c:1646  */
    {;}
#line 1528 "y.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 86 "cpyrr.y" /* yacc.c:1646  */
    {;}
#line 1534 "y.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 90 "cpyrr.y" /* yacc.c:1646  */
    {;}
#line 1540 "y.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 93 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_LISTE_INST,EMPTY,EMPTY),(yyvsp[-1].struct_arbre).a);}
#line 1546 "y.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 94 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = concat_fils_frere((yyvsp[-2].struct_arbre).a,concat_pere_fils(creer_noeud(A_LISTE_INST,EMPTY,EMPTY),(yyvsp[-2].struct_arbre).a));}
#line 1552 "y.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 99 "cpyrr.y" /* yacc.c:1646  */
    {;}
#line 1558 "y.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 103 "cpyrr.y" /* yacc.c:1646  */
    {
    if ((yyvsp[0].struct_simple).valeur.i == STRCT){
        indice = ajouter_declaration((yyvsp[-2].struct_simple).valeur.i,(yyvsp[0].struct_simple).valeur.i,sommet(p),ajouter_structure(nbr_champs/3,champs));
    }
    else{
        int tab = ajouter_tableaux((yyvsp[0].struct_simple).type,nbr_dimensions/2,dimensions);
        indice = ajouter_declaration((yyvsp[-2].struct_simple).valeur.i,TAB,sommet(p),tab);
    }
      nbr_dimensions = 0;
      nbr_champs = 0;}
#line 1573 "y.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 115 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_simple).valeur.i = STRCT;}
#line 1579 "y.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 116 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_simple).type = (yyvsp[0].struct_simple).type ,(yyval.struct_simple).valeur.i = (yyvsp[0].struct_simple).valeur.i;}
#line 1585 "y.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 133 "cpyrr.y" /* yacc.c:1646  */
    {dimensions[nbr_dimensions++] = (yyvsp[-2].struct_arbre).valeur.i; dimensions[nbr_dimensions++] = (yyvsp[0].struct_arbre).valeur.i; (yyval.struct_simple).valeur.i = (yyvsp[0].struct_arbre).valeur.i - (yyvsp[-2].struct_arbre).valeur.i;}
#line 1591 "y.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 140 "cpyrr.y" /* yacc.c:1646  */
    {champs[nbr_champs++] = (yyvsp[0].struct_simple).type; champs[nbr_champs++] = (yyvsp[-2].struct_simple).valeur.i; champs[nbr_champs++] = 1; (yyval.struct_simple).valeur.i = (yyvsp[0].struct_simple).type; }
#line 1597 "y.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 143 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_simple).type = (yyvsp[0].struct_simple).type;}
#line 1603 "y.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 144 "cpyrr.y" /* yacc.c:1646  */
    {if((test_assoc = assoc_nom((yyvsp[0].struct_simple).valeur.i)) != -1 ) (yyval.struct_simple).type = assoc_nom(test_assoc); else printf("Erreur : Le type %d n'existe pas ligne %d\n",(yyvsp[0].struct_simple).valeur.i,numligne);}
#line 1609 "y.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 147 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_simple).type = 0;}
#line 1615 "y.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 148 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_simple).type = 1;}
#line 1621 "y.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 149 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_simple).type = 3;}
#line 1627 "y.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 150 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_simple).type = 2;}
#line 1633 "y.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 151 "cpyrr.y" /* yacc.c:1646  */
    {;}
#line 1639 "y.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 154 "cpyrr.y" /* yacc.c:1646  */
    { indice = ajouter_declaration((yyvsp[-2].struct_simple).valeur.i,VAR,sommet(p),assoc_nom((yyvsp[0].struct_simple).type),assoc_nom((yyvsp[0].struct_simple).type)); modifier_taille_region(region,tab_declarations[indice].execution);}
#line 1645 "y.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 157 "cpyrr.y" /* yacc.c:1646  */
    {indice = ajouter_declaration((yyvsp[-4].struct_simple).valeur.i,PROC,sommet(p),ajouter_procedure(nbr_params/2,params),region);nbr_params = 0;}
#line 1651 "y.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 160 "cpyrr.y" /* yacc.c:1646  */
    {indice = ajouter_declaration((yyvsp[-6].struct_simple).valeur.i,FCT,sommet(p),ajouter_fonction((yyvsp[-3].struct_simple).type,nbr_params/2,params),region);nbr_params=0;}
#line 1657 "y.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 171 "cpyrr.y" /* yacc.c:1646  */
    {params[nbr_params++] = (yyvsp[-2].struct_simple).valeur.i; params[nbr_params++] = (yyvsp[0].struct_simple).type; indice = ajouter_declaration((yyvsp[-2].struct_simple).valeur.i,PARAM,sommet(p),assoc_nom((yyvsp[0].struct_simple).type),(yyvsp[0].struct_simple).type);}
#line 1663 "y.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 174 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 1669 "y.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 175 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 1675 "y.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 176 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 1681 "y.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 177 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a; memset(arg_fonction,-1,sizeof(int)*MAX_ARG);nb_arg_fonction;}
#line 1687 "y.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 178 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = creer_noeud(A_VIDE,EMPTY,EMPTY);}
#line 1693 "y.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 179 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_RETURN,EMPTY,EMPTY),(yyvsp[0].struct_arbre).a);}
#line 1699 "y.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 183 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = creer_arbre_vide();}
#line 1705 "y.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 184 "cpyrr.y" /* yacc.c:1646  */
    { (yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 1711 "y.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 193 "cpyrr.y" /* yacc.c:1646  */
    {
    if (assoc_nom((yyvsp[-1].struct_simple).valeur.i) != -1){
        (yyval.struct_arbre).type = representations[tab_declarations[assoc_nom((yyvsp[-1].struct_simple).valeur.i)].description];
        (yyval.struct_arbre).a = creer_arbre_vide();
        if (verife_arg_fonction((yyvsp[-1].struct_simple).valeur.i) ){
            if (tab_declarations[assoc_nom((yyvsp[-1].struct_simple).valeur.i)].nature == FCT ){
                (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_FONCTION,EMPTY,EMPTY),(yyvsp[0].struct_arbre).a);
            }
            else if(tab_declarations[assoc_nom((yyvsp[-1].struct_simple).valeur.i)].nature == PROC){
                (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_PROCEDURE,EMPTY,EMPTY),(yyvsp[0].struct_arbre).a);
            }
        }
    }
    else{
        printf("Erreur : L'appel n'est pas fait sur une fonction ou procedure déclaré ligne %d\n",numligne);
        (yyval.struct_arbre).a = creer_arbre_vide();
    }
  }
#line 1734 "y.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 213 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = (yyvsp[-1].struct_arbre).a;}
#line 1740 "y.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 216 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = creer_arbre_vide();}
#line 1746 "y.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 217 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_LISTE_ARGS,EMPTY,EMPTY),(yyvsp[0].struct_arbre).a);}
#line 1752 "y.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 218 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = concat_fils_frere((yyvsp[-2].struct_arbre).a,concat_pere_fils(creer_noeud(A_LISTE_ARGS,EMPTY,EMPTY),(yyvsp[0].struct_arbre).a));}
#line 1758 "y.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 221 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;arg_fonction[nb_arg_fonction] = (yyvsp[0].struct_arbre).type;nb_arg_fonction++;}
#line 1764 "y.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 224 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_IF_THEN_ELSE,EMPTY,EMPTY),concat_fils_frere((yyvsp[-4].struct_arbre).a,concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a)));}
#line 1770 "y.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 232 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_WHILE,EMPTY,EMPTY),concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}
#line 1776 "y.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 235 "cpyrr.y" /* yacc.c:1646  */
    {
    if((yyvsp[-2].struct_arbre).type != (yyvsp[0].struct_arbre).type){printf("Erreur : Probleme d'affectation ligne %d\n",numligne);
        (yyval.struct_arbre).a = creer_arbre_vide();
    }
    else{
        if ((yyvsp[-2].struct_arbre).type == T_ENTIER)
            (yyvsp[-2].struct_arbre).valeur.i = (yyvsp[0].struct_arbre).valeur.i;
        else if ((yyvsp[-2].struct_arbre).type == T_REEL)
            (yyvsp[-2].struct_arbre).valeur.f = (yyvsp[0].struct_arbre).valeur.f;
        else if ((yyvsp[-2].struct_arbre).type == T_CHAR)
            (yyvsp[-2].struct_arbre).valeur.c = (yyvsp[0].struct_arbre).valeur.c;
        else
            (yyvsp[-2].struct_arbre).valeur.s = (yyvsp[0].struct_arbre).valeur.s;
        (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_OPAFF,EMPTY,EMPTY),concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}
  }
#line 1796 "y.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 252 "cpyrr.y" /* yacc.c:1646  */
    {
    printf("Dans variable\n");
    decl_associe=assoc_nom((yyvsp[-1].struct_simple).valeur.i);
    if (decl_associe == -1){
        printf ("Erreur : Variable non déclaré ligne %d\n",numligne);
        (yyval.struct_arbre).a = creer_arbre_vide();
    }
    else if (tab_declarations[decl_associe].nature != VAR){
        printf ("Erreur : Ce n'est pas une variable ligne %d\n",numligne);
        (yyval.struct_arbre).a = creer_arbre_vide();
    }
    else{
        (yyval.struct_arbre).type = tab_declarations[assoc_nom((yyvsp[-1].struct_simple).valeur.i)].description;
        if(tab_declarations[(yyval.struct_arbre).type].nature == TAB ) {
            (yyval.struct_arbre).type = representations[tab_declarations[(yyval.struct_arbre).type].description];
            verifier_dimensions(tab_declarations[assoc_nom((yyvsp[-1].struct_simple).valeur.i)].description);
        }
        (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_IDF,(yyvsp[-1].struct_simple).valeur.i,assoc_nom((yyvsp[-1].struct_simple).valeur.i)),(yyvsp[0].struct_arbre).a);}
  }
#line 1820 "y.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 273 "cpyrr.y" /* yacc.c:1646  */
    {if (est_vide((yyvsp[0].struct_arbre).a)){(yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_TAB,EMPTY,EMPTY),(yyvsp[-2].struct_arbre).a);}else{(yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_STRUCT,EMPTY,EMPTY),concat_fils_frere(creer_noeud(A_TAB,EMPTY,EMPTY),(yyvsp[0].struct_arbre).a));};}
#line 1826 "y.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 274 "cpyrr.y" /* yacc.c:1646  */
    {if (est_vide((yyvsp[0].struct_arbre).a)){(yyval.struct_arbre).a = creer_arbre_vide();}else{(yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_STRUCT,EMPTY,EMPTY),(yyvsp[0].struct_arbre).a);};}
#line 1832 "y.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 277 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 1838 "y.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 278 "cpyrr.y" /* yacc.c:1646  */
    {printf("OK ??????\n");(yyval.struct_arbre).a = creer_arbre_vide();}
#line 1844 "y.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 281 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_LISTE_INDICE,EMPTY,EMPTY),(yyvsp[0].struct_arbre).a); verif_dimensions[nb_dim_verif++] = (yyvsp[0].struct_arbre).valeur.i;}
#line 1850 "y.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 282 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).a = concat_pere_fils((yyvsp[-2].struct_arbre).a,concat_fils_frere(creer_noeud(A_LISTE_INDICE,EMPTY,EMPTY),(yyvsp[0].struct_arbre).a));verif_dimensions[nb_dim_verif++] = (yyvsp[0].struct_arbre).valeur.i;}
#line 1856 "y.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 286 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).type = (yyvsp[0].struct_arbre).type; (yyval.struct_arbre).valeur = (yyvsp[0].struct_arbre).valeur; (yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 1862 "y.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 287 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).type = T_BOOL; (yyval.struct_arbre).valeur = (yyvsp[0].struct_arbre).valeur; (yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 1868 "y.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 290 "cpyrr.y" /* yacc.c:1646  */
    {printf("Dans le ||\n");(yyval.struct_arbre).type = T_BOOL; (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i || (yyvsp[0].struct_arbre).valeur.i; (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_OU,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}
#line 1874 "y.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 291 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).type = T_BOOL; (yyval.struct_arbre).valeur.i = (yyvsp[0].struct_arbre).valeur.i; (yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 1880 "y.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 294 "cpyrr.y" /* yacc.c:1646  */
    {printf("Dans le &&\n");(yyval.struct_arbre).type = T_BOOL; (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i && (yyvsp[0].struct_arbre).valeur.i; (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_ET,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}
#line 1886 "y.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 295 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).type = T_BOOL; (yyval.struct_arbre).valeur.i = (yyvsp[0].struct_arbre).valeur.i; (yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 1892 "y.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 298 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).type = T_BOOL; if ((yyvsp[0].struct_arbre).valeur.i == 0){ (yyval.struct_arbre).valeur.i = 1; (yyval.struct_arbre).a = creer_arbre_vide(); } else (yyval.struct_arbre).valeur.i = 0; (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_NON,EMPTY,EMPTY), (yyvsp[0].struct_arbre).a);}
#line 1898 "y.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 299 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).type = T_BOOL; (yyval.struct_arbre).valeur.i = (yyvsp[0].struct_arbre).valeur.i; (yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 1904 "y.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 302 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).type = T_BOOL; (yyval.struct_arbre).valeur.i = (yyvsp[-1].struct_arbre).valeur.i; (yyval.struct_arbre).a = (yyvsp[-1].struct_arbre).a;}
#line 1910 "y.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 303 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).valeur.i = 1; (yyval.struct_arbre).type = T_BOOL; (yyval.struct_arbre).a = creer_noeud(A_VRAI,EMPTY,EMPTY);}
#line 1916 "y.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 304 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).valeur.i = 0; (yyval.struct_arbre).type = T_BOOL; (yyval.struct_arbre).a = creer_noeud(A_FAUX,EMPTY,EMPTY);}
#line 1922 "y.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 305 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).type = (yyvsp[0].struct_arbre).type; (yyval.struct_arbre).valeur.i = (yyvsp[0].struct_arbre).valeur.i; (yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 1928 "y.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 309 "cpyrr.y" /* yacc.c:1646  */
    {
    printf("Dans OPEG\n");
    if ((yyvsp[-2].struct_arbre).type != (yyvsp[0].struct_arbre).type){
        printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
        (yyval.struct_arbre).a = creer_arbre_vide();
    }
    else{
        (yyval.struct_arbre).type = T_BOOL;
        if((yyvsp[-2].struct_arbre).type == T_ENTIER)
            (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i == (yyvsp[0].struct_arbre).valeur.i;
        else if((yyvsp[-2].struct_arbre).type == T_REEL) (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.f == (yyvsp[0].struct_arbre).valeur.f;
        else if((yyvsp[-2].struct_arbre).type == T_CHAR) (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.c == (yyvsp[0].struct_arbre).valeur.c;
        else printf("Erreur : Comparaison impossible ligne %d",numligne);
        (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_OPEG,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}
  }
#line 1948 "y.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 326 "cpyrr.y" /* yacc.c:1646  */
    {
                if ((yyvsp[-2].struct_arbre).type != (yyvsp[0].struct_arbre).type){
                    printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
                    (yyval.struct_arbre).a = creer_arbre_vide();
                }
                else{
                    (yyval.struct_arbre).type = T_BOOL;
                    if((yyvsp[-2].struct_arbre).type == T_ENTIER)
                        (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i == (yyvsp[0].struct_arbre).valeur.i;
                    else if((yyvsp[-2].struct_arbre).type == T_REEL) (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.f == (yyvsp[0].struct_arbre).valeur.f;
                    else if((yyvsp[-2].struct_arbre).type == T_CHAR) (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.c == (yyvsp[0].struct_arbre).valeur.c; 
                    else printf("Erreur : Comparaison impossible ligne %d",numligne);
                    (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_OPDIF,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}}
#line 1966 "y.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 341 "cpyrr.y" /* yacc.c:1646  */
    {
                if ((yyvsp[-2].struct_arbre).type != (yyvsp[0].struct_arbre).type){
                    printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
                    (yyval.struct_arbre).a = creer_arbre_vide();
                }
                else{
                    (yyval.struct_arbre).type = T_BOOL;
                    if((yyvsp[-2].struct_arbre).type == T_ENTIER)
                        (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i == (yyvsp[0].struct_arbre).valeur.i;
                    else if((yyvsp[-2].struct_arbre).type == T_REEL) (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.f == (yyvsp[0].struct_arbre).valeur.f;
                    else if((yyvsp[-2].struct_arbre).type == T_CHAR) (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.c == (yyvsp[0].struct_arbre).valeur.c; 
                    else printf("Erreur :Comparaison impossible ligne %d",numligne);   
                    (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_OPINF,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}}
#line 1984 "y.tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 356 "cpyrr.y" /* yacc.c:1646  */
    {
                if ((yyvsp[-2].struct_arbre).type != (yyvsp[0].struct_arbre).type){
                    printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
                    (yyval.struct_arbre).a = creer_arbre_vide();
                }
                else{
                    (yyval.struct_arbre).type = T_BOOL;
                    if((yyvsp[-2].struct_arbre).type == T_ENTIER)
                        (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i == (yyvsp[0].struct_arbre).valeur.i;
                    else if((yyvsp[-2].struct_arbre).type == T_REEL) (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.f == (yyvsp[0].struct_arbre).valeur.f;
                    else if((yyvsp[-2].struct_arbre).type == T_CHAR) (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.c == (yyvsp[0].struct_arbre).valeur.c;
                    else printf("Erreur : Comparaison impossible ligne %d",numligne);
                    (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_OPSUP,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}}
#line 2002 "y.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 371 "cpyrr.y" /* yacc.c:1646  */
    {
                if ((yyvsp[-2].struct_arbre).type != (yyvsp[0].struct_arbre).type){
                    printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
                    (yyval.struct_arbre).a = creer_arbre_vide();
                }
                else{
                    (yyval.struct_arbre).type = T_BOOL;
                    if((yyvsp[-2].struct_arbre).type == T_ENTIER)
                        (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i == (yyvsp[0].struct_arbre).valeur.i;
                    else if((yyvsp[-2].struct_arbre).type == T_REEL) (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.f == (yyvsp[0].struct_arbre).valeur.f;
                    else if((yyvsp[-2].struct_arbre).type == T_CHAR) (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.c == (yyvsp[0].struct_arbre).valeur.c;
                    else printf("Erreur : Comparaison impossible ligne %d",numligne);
                    (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_OPINFEG,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}}
#line 2020 "y.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 386 "cpyrr.y" /* yacc.c:1646  */
    {
                if ((yyvsp[-2].struct_arbre).type != (yyvsp[0].struct_arbre).type){
                    printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
                    (yyval.struct_arbre).a = creer_arbre_vide();
                }
                else{(yyval.struct_arbre).type = T_BOOL;
                    if((yyvsp[-2].struct_arbre).type == T_ENTIER)
                        (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i == (yyvsp[0].struct_arbre).valeur.i;
                    else if((yyvsp[-2].struct_arbre).type == T_REEL) (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.f == (yyvsp[0].struct_arbre).valeur.f;
                    else if((yyvsp[-2].struct_arbre).type == T_CHAR) (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.c == (yyvsp[0].struct_arbre).valeur.c;
                    else printf("Comparaison impossible ligne %d",numligne);
                    (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_OPSUPEG,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}}
#line 2037 "y.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 401 "cpyrr.y" /* yacc.c:1646  */
    {
    if ((yyvsp[-2].struct_arbre).type != (yyvsp[0].struct_arbre).type){
        printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
        (yyval.struct_arbre).a = creer_arbre_vide();
    }
      else{(yyval.struct_arbre).type = (yyvsp[-2].struct_arbre).type; 
          if((yyvsp[-2].struct_arbre).type == T_ENTIER)
              (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i + (yyvsp[0].struct_arbre).valeur.i;
          else if((yyvsp[-2].struct_arbre).type == T_REEL)
              (yyval.struct_arbre).valeur.f = (yyvsp[-2].struct_arbre).valeur.f + (yyvsp[0].struct_arbre).valeur.f;
          else if((yyvsp[-2].struct_arbre).type == T_CHAR)
              (yyval.struct_arbre).valeur.c = (yyvsp[-2].struct_arbre).valeur.c + (yyvsp[0].struct_arbre).valeur.c;
          else if((yyvsp[-2].struct_arbre).type == T_STRING)
              (yyval.struct_arbre).valeur.s = strcat((yyvsp[-2].struct_arbre).valeur.s,(yyvsp[0].struct_arbre).valeur.s);
          else
              printf("Erreur : Addition impossible pour ce type ligne %d\n",numligne);
          (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_PLUS,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}}
#line 2059 "y.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 420 "cpyrr.y" /* yacc.c:1646  */
    {
       if ((yyvsp[-2].struct_arbre).type != (yyvsp[0].struct_arbre).type){
           printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
           (yyval.struct_arbre).a = creer_arbre_vide();
       }
      else{(yyval.struct_arbre).type = (yyvsp[-2].struct_arbre).type;
          if((yyvsp[-2].struct_arbre).type == T_ENTIER)
              (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i - (yyvsp[0].struct_arbre).valeur.i;
          else if((yyvsp[-2].struct_arbre).type == T_REEL)
              (yyval.struct_arbre).valeur.f = (yyvsp[-2].struct_arbre).valeur.f - (yyvsp[0].struct_arbre).valeur.f;
          else
              printf("Erreur : Soustraction impossible pour ce type ligne %d \n",numligne);
          (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_MOINS,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}}
#line 2077 "y.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 435 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).type = (yyvsp[0].struct_arbre).type; (yyval.struct_arbre).valeur = (yyvsp[0].struct_arbre).valeur; (yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 2083 "y.tab.c" /* yacc.c:1646  */
    break;

  case 86:
#line 438 "cpyrr.y" /* yacc.c:1646  */
    {
    if ((yyvsp[-2].struct_arbre).type != (yyvsp[0].struct_arbre).type){
        printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
        (yyval.struct_arbre).a = creer_arbre_vide();
    }
    else{(yyval.struct_arbre).type = (yyvsp[-2].struct_arbre).type;
        if((yyvsp[-2].struct_arbre).type == T_ENTIER)
            (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i * (yyvsp[0].struct_arbre).valeur.i;
        else if((yyvsp[-2].struct_arbre).type == T_REEL)
            (yyval.struct_arbre).valeur.f = (yyvsp[-2].struct_arbre).valeur.f * (yyvsp[0].struct_arbre).valeur.f;
        else
            printf("Erreur : Multiplication impossible pour ce type ligne %d \n",numligne);
        (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_MULT,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}}
#line 2101 "y.tab.c" /* yacc.c:1646  */
    break;

  case 87:
#line 454 "cpyrr.y" /* yacc.c:1646  */
    {
        if ((yyvsp[-2].struct_arbre).type != (yyvsp[0].struct_arbre).type){
            printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
            (yyval.struct_arbre).a = creer_arbre_vide();
        }
        else{(yyval.struct_arbre).type = (yyvsp[-2].struct_arbre).type;
            if((yyvsp[-2].struct_arbre).type == T_ENTIER){
                if ((yyvsp[0].struct_arbre).valeur.i == 0) printf("Erreur : Division par 0 impossible ligne %d\n",numligne); 
                (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i / (yyvsp[0].struct_arbre).valeur.i;
            }
            else if((yyvsp[-2].struct_arbre).type == T_REEL){
                if ((yyvsp[0].struct_arbre).valeur.f == 0.0) printf("Erreur : Division par 0 impossible ligne %d\n",numligne); 
                (yyval.struct_arbre).valeur.f = (yyvsp[-2].struct_arbre).valeur.f / (yyvsp[0].struct_arbre).valeur.f;
            }
            else
                printf("Erreur : Division impossible pour ce type ligne %d \n",numligne);
            (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_DIV,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}}
#line 2123 "y.tab.c" /* yacc.c:1646  */
    break;

  case 88:
#line 473 "cpyrr.y" /* yacc.c:1646  */
    {
        if ((yyvsp[-2].struct_arbre).type != (yyvsp[0].struct_arbre).type){
            printf("Erreur : les types des operandes sont different à la ligne %d\n",numligne);
            (yyval.struct_arbre).a = creer_arbre_vide();
        }
        else{(yyval.struct_arbre).type = (yyvsp[-2].struct_arbre).type;
            if((yyvsp[-2].struct_arbre).type == T_ENTIER)
                (yyval.struct_arbre).valeur.i = (yyvsp[-2].struct_arbre).valeur.i % (yyvsp[0].struct_arbre).valeur.i;
            else
                printf("Erreur : Modulo impossible pour ce type ligne %d \n",numligne);
            (yyval.struct_arbre).a = concat_pere_fils(creer_noeud(A_MOD,EMPTY,EMPTY), concat_fils_frere((yyvsp[-2].struct_arbre).a,(yyvsp[0].struct_arbre).a));}}
#line 2139 "y.tab.c" /* yacc.c:1646  */
    break;

  case 89:
#line 486 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).valeur = (yyvsp[0].struct_arbre).valeur; (yyval.struct_arbre).type = (yyvsp[0].struct_arbre).type; (yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 2145 "y.tab.c" /* yacc.c:1646  */
    break;

  case 90:
#line 489 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).valeur = (yyvsp[-1].struct_arbre).valeur; (yyval.struct_arbre).type = (yyvsp[-1].struct_arbre).type; (yyval.struct_arbre).a = (yyvsp[-1].struct_arbre).a;}
#line 2151 "y.tab.c" /* yacc.c:1646  */
    break;

  case 91:
#line 490 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).valeur.i = (yyvsp[0].struct_simple).valeur.i; (yyval.struct_arbre).type = T_ENTIER; (yyval.struct_arbre).a = creer_noeud(A_INT, EMPTY, EMPTY);}
#line 2157 "y.tab.c" /* yacc.c:1646  */
    break;

  case 92:
#line 491 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).valeur.f = (yyvsp[0].struct_simple).valeur.f; (yyval.struct_arbre).type = T_REEL;(yyval.struct_arbre).a =  creer_noeud(A_FLOAT,EMPTY, EMPTY);}
#line 2163 "y.tab.c" /* yacc.c:1646  */
    break;

  case 93:
#line 492 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).valeur.s = (yyvsp[0].struct_simple).valeur.s; (yyval.struct_arbre).type = T_STRING; (yyval.struct_arbre).a = creer_noeud(A_CHAINE,EMPTY,EMPTY);}
#line 2169 "y.tab.c" /* yacc.c:1646  */
    break;

  case 94:
#line 493 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).valeur.c = (yyvsp[0].struct_simple).valeur.c; (yyval.struct_arbre).type = T_CHAR;(yyval.struct_arbre).a = creer_noeud(A_CHAR,EMPTY,EMPTY);}
#line 2175 "y.tab.c" /* yacc.c:1646  */
    break;

  case 95:
#line 494 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).type = (yyvsp[0].struct_arbre).type; if ((yyval.struct_arbre).type == T_ENTIER) (yyval.struct_arbre).valeur.i = (yyvsp[0].struct_arbre).valeur.i; else if((yyval.struct_arbre).type == T_REEL)(yyval.struct_arbre).valeur.f = (yyvsp[0].struct_arbre).valeur.f; else if((yyval.struct_arbre).type == T_CHAR)(yyval.struct_arbre).valeur.c = (yyvsp[0].struct_arbre).valeur.c; else (yyval.struct_arbre).valeur.s = (yyvsp[0].struct_arbre).valeur.s; (yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;}
#line 2181 "y.tab.c" /* yacc.c:1646  */
    break;

  case 96:
#line 495 "cpyrr.y" /* yacc.c:1646  */
    {(yyval.struct_arbre).type = (yyvsp[0].struct_arbre).type; if ((yyval.struct_arbre).type == T_ENTIER) (yyval.struct_arbre).valeur.i = (yyvsp[0].struct_arbre).valeur.i; else if((yyval.struct_arbre).type == T_REEL)(yyval.struct_arbre).valeur.f = (yyvsp[0].struct_arbre).valeur.f; else if((yyval.struct_arbre).type == T_CHAR)(yyval.struct_arbre).valeur.c = (yyvsp[0].struct_arbre).valeur.c; else (yyval.struct_arbre).valeur.s = (yyvsp[0].struct_arbre).valeur.s; (yyval.struct_arbre).a = (yyvsp[0].struct_arbre).a;memset(arg_fonction,-1,sizeof(int)*MAX_ARG);nb_arg_fonction;}
#line 2187 "y.tab.c" /* yacc.c:1646  */
    break;


#line 2191 "y.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 499 "cpyrr.y" /* yacc.c:1906  */

int yyerror(){
printf("Erreur de syntaxe en ligne %d\n", numligne);
return -1 ;
}

int verife_arg_fonction(int num_lexico){
    int i;
    int type = tab_declarations[assoc_nom(num_lexico)].description + 1;
    int res = 0;
    /*verife nombre d'arguments */
    if(nb_arg_fonction != representations[type]){
        printf("Erreur : Le nombre d'argument de la fonction %s en ligne %d est different de la declaration\n",tb_lex[num_lexico].lexem, numligne);
        res = -1;
    }
    /* verifie le type des arguments */
    else {
        for(i=0; i<nb_arg_fonction; i++){
            type += 2;
            if(arg_fonction[i] != representations[type]){
                printf("Erreur : L'argument %d de la fonction %s en ligne %d est different de la declaration\n",i+1,tb_lex[num_lexico].lexem, numligne);
                res = -1;
            }
        }
    }
    return res;
}

int verifier_dimensions (int num_dec_type){
    int i;
    int res;
    int verife = (tab_declarations[num_dec_type].description) + 1;
/*verifie nombre de dimensions*/
    if(nb_dim_verif != representations[verife]){
        printf("Erreur : Le nombre %d de dimensions est incorect ligne %d\n",nb_dim_verif,numligne);
        res -1;
    }
    else{
        /*verifie la valeur de la dimension*/
        for(i = 0; i < nb_dim_verif; i++){
            verife ++;
            if (verif_dimensions[i] < representations[verife]){
                printf("Erreur : La valeur de la dimension %d est inccorect\n",i);
                res = -1;
            }
            verife ++;
            if (verif_dimensions[i] > representations[verife]){
                printf("Erreur : La valeur de la dimension %d est inccorect\n",i);
                res = 0;
            }
        } 
    }
    return res;
}

int main(){
yyparse() ;
affichage_lexeme();
affiche_declarations();
affiche_representation();
afficher_region();
}
