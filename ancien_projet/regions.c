#include"regions.h"
#include<string.h>

//Fonctions concernant la table des regions

void initialiser_region(){
    int i;
    for(i=0; i<TAILLE_MAX; i++){
        tab_region[i].taille = -1;
        tab_region[i].NIS = -1;
        tab_region[i].arbre = -1;
    }
}

void ajouter_region(int indice, int taille, int NIS , int arbre){
    tab_region[indice].taille = taille;
    tab_region[indice].NIS = NIS;
    tab_region[indice].arbre = arbre;
}

void afficher_region(){
    int i;
    printf("\nTable des regions \n");
    printf(" Taille |  NIS  |  Arbre\n");
    for(i=0;i<TAILLE_MAX;i++){
        printf("   %d   |  %d   |  %d\n", tab_region[i].taille, tab_region[i].NIS, tab_region[i].arbre);
    }
    printf("\n");
           
}

//Fonctions concernant les piles de region

int pile_vide(pile p1){
    return (p1.t_pile[0] == -1);
}

void init_pile(pile p1){
    int i;
    for(i=0;i<TAILLE_MAX;i++)
        p1.t_pile[i] = -1;
}

void empiler(pile p1, int region){
    p1.t_pile[++p1.sommet_pile] = region;
}


void depiler(pile p1){
    p1.t_pile[p1.sommet_pile--] = -1;
}

int sommet(pile p1){
    return p1.sommet_pile;
}

void affiche_pile(pile p1){
    int i;
    printf("\nPile des regions\n");
    for(i=0;i<p1.sommet_pile;i++){
        printf(" %d | ", p1.t_pile[i]); 
    }
    printf("\n");
}