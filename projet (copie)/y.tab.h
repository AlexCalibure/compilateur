/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    PROG = 258,
    PV = 259,
    DEBUT = 260,
    FIN = 261,
    TYPE = 262,
    DP = 263,
    STRUCT = 264,
    FSTRUCT = 265,
    TABLEAU = 266,
    FLECHE = 267,
    ENTIER = 268,
    REEL = 269,
    BOOLEEN = 270,
    CARACTERE = 271,
    RETOURNE = 272,
    PO = 273,
    PF = 274,
    VIDE = 275,
    SI = 276,
    ALORS = 277,
    SINON = 278,
    TANT_QUE = 279,
    FAIRE = 280,
    OPAFF = 281,
    CO = 282,
    CF = 283,
    P = 284,
    V = 285,
    OU = 286,
    ET = 287,
    NON = 288,
    VRAI = 289,
    FAUX = 290,
    OPEG = 291,
    OPDIF = 292,
    OPINF = 293,
    OPSUP = 294,
    OPINFEG = 295,
    OPSUPEG = 296,
    PLUS = 297,
    MOINS = 298,
    MULT = 299,
    DIV = 300,
    MOD = 301,
    NEG = 302,
    VARIABLE = 303,
    PROCEDURE = 304,
    FONCTION = 305,
    BEG = 306,
    END = 307,
    P_P = 308,
    IDF = 309,
    CSTE_ENTIERE = 310,
    CSTE_REELE = 311,
    CHAINE = 312,
    UN_CARACTERE = 313
  };
#endif
/* Tokens.  */
#define PROG 258
#define PV 259
#define DEBUT 260
#define FIN 261
#define TYPE 262
#define DP 263
#define STRUCT 264
#define FSTRUCT 265
#define TABLEAU 266
#define FLECHE 267
#define ENTIER 268
#define REEL 269
#define BOOLEEN 270
#define CARACTERE 271
#define RETOURNE 272
#define PO 273
#define PF 274
#define VIDE 275
#define SI 276
#define ALORS 277
#define SINON 278
#define TANT_QUE 279
#define FAIRE 280
#define OPAFF 281
#define CO 282
#define CF 283
#define P 284
#define V 285
#define OU 286
#define ET 287
#define NON 288
#define VRAI 289
#define FAUX 290
#define OPEG 291
#define OPDIF 292
#define OPINF 293
#define OPSUP 294
#define OPINFEG 295
#define OPSUPEG 296
#define PLUS 297
#define MOINS 298
#define MULT 299
#define DIV 300
#define MOD 301
#define NEG 302
#define VARIABLE 303
#define PROCEDURE 304
#define FONCTION 305
#define BEG 306
#define END 307
#define P_P 308
#define IDF 309
#define CSTE_ENTIERE 310
#define CSTE_REELE 311
#define CHAINE 312
#define UN_CARACTERE 313

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 29 "cpyrr.y" /* yacc.c:1909  */

    struct{
        arbre a;
        int type;
        union{
            int i;
            char c;
            float f;
            char *s;
        }valeur;
    }struct_arbre;
    struct{
        int type;
        union{
            int i;
            char c;
            float f;
            char *s;
        }valeur;
    }struct_simple;

#line 192 "y.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
