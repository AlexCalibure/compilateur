#include "declarations.h"
#include "table_lexico.h"
#include <string.h>

void init_declarations (){
    int i;
    for ( i = 0; i < 4; i++){
        tab_declarations[i].nature = BASE;
        tab_declarations[i].suivant = -1;
        tab_declarations[i].region = -1;
        tab_declarations[i].description = i;
        //tab_declarations[i].execution = 0;
    }
    for ( i = 4; i < TAILLE; i++){
        tab_declarations[i].nature = RIEN;
        tab_declarations[i].suivant = -1;
        tab_declarations[i].region = -1;
        tab_declarations[i].description = -1;
        //tab_declarations[i].execution = 0;
    }
}

void ajouter_declaration (int indice, int nature, int region){
    //Si c'est la premiere declaration d'un lexeme.
    if (tab_declarations[indice].nature == RIEN ){
        tab_declarations[indice].nature = nature;
        tab_declarations[indice].region = region;
        //tab_declarations[indice].description = description;
        //tab_declarations[indice].execution = execution;
    }

    //Si ce n'est pas la premiere declaration du lexeme.
    else{
        int limite = LIMITE;
        while (tab_declarations[limite].nature != RIEN){   //On cherche un espace libre dans la seconde partie de la table des declarations pour faire la nouvelle declaration.
            limite ++;
            if ( limite == TAILLE ){  //On verifie qu'on ne dépasse pas la taille du tableau.
                printf("Trop de déclaration !\n");
                exit(-1);
            }
        }
        tab_declarations[indice].suivant = limite;
        tab_declarations[limite].nature = nature;
        tab_declarations[limite].region = region;
        //tab_declarations[limite].description = description;
        //tab_declarations[indice].execution = execution;
    }
}

void affiche_declarations(){
    int i;
    printf("\nTable des declarations\n__________________________________________________________________________________\n");
    printf("Indice    |    Nature   |  Suivant   |   Region   |   Description  |    Execution\n");
    printf("__________________________________________________________________________________\n");
    for (i = 0; i < TAILLE; i++){
    printf("  %d      |    %d       |    %d      |    %d      |                |\n",tab_declarations[i].nature, tab_declarations[i].suivant, tab_declarations[i].region, tab_declarations[i].description);
    }
}
