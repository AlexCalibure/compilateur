#include"arbre_abstrait.h"

int est_vide(arbre a){
  if ( a == NULL){
    return 1;
  }
  return 0;
}

arbre creer_arbre_vide(){
  return NULL;
}

/* init un arbre avec 2 fils vide */
void init_arbre(arbre a){
    a->nature = EMPTY;
    a->fils_gauche = NULL;
    a->frere_droit = NULL;
}

/* cree un noeud et remplie la structure */
arbre creer_noeud (int nature, int num_lex, int num_dec){
  arbre a = (arbre) malloc (sizeof(noeud));
  a->nature = nature;
  a->num_lex = num_lex;
  a->num_dec = num_dec;
  a->fils_gauche=creer_arbre_vide();
  a->frere_droit=creer_arbre_vide();
  return a;
}

/* cree un arbre avec des fils mais valuer vide */
arbre creer_arbre (int nature, arbre fils_gauche, arbre frere_droit){
    arbre a = creer_noeud(nature,EMPTY,EMPTY);
    a->fils_gauche = fils_gauche;
    a->frere_droit = frere_droit;
    return a;
}


arbre concat_pere_fils (arbre a, arbre fils_gauche){
    a->fils_gauche = fils_gauche;
    return a;
}

arbre concat_fils_frere (arbre a, arbre frere_droit){
    a->frere_droit = frere_droit;
    return a;
}


void afficher_arbre(arbre a){
    printf("%d\n ",a->nature);
    if ( !est_vide(a->fils_gauche) ){
        afficher_arbre(a->fils_gauche);
    }
    if ( !est_vide(a->frere_droit)){
        afficher_arbre(a->frere_droit);
    }
}

/*
int evalue_arbre_exp(arbre a){
  int val1, val2;
  switch(a->nature){
  case A_PLUS :
      val1 = evalue_arbre_exp(a->fils_gauche);
      val2 = evalue_arbre_exp(a->fils_gauche->frere_droit);
      return (val1 + val2);
      break;
  case A_MOINS :
      val1 = evalue_arbre_exp(a->fils_gauche);
      val2 = evalue_arbre_exp(a->fils_gauche->frere_droit);
      return (val1-val2);
      break;
  case A_MULT : 
      val1 = evalue_arbre_exp(a->fils_gauche);
      val2 = evalue_arbre_exp(a->fils_gauche->frere_droit);
      return (val1*val2);
      break;
  case A_DIV : 
      val1 = evalue_arbre_exp(a->fils_gauche);
      val2 = evalue_arbre_exp(a->fils_gauche->frere_droit);
      return (val1/val2);
      break;
  case A_MOD :
      val1 = evalue_arbre_exp(a->fils_gauche);
      val2 = evalue_arbre_exp(a->fils_gauche->frere_droit);
      return (val1%val2);
      break;
  case A_IDF :
      break;
  case A_FONCTION :
      break;
  case A_PROCEDURE :
      break;
  default:
      break;
  }
  return 0;
  }

int evalue_arbre_bool(arbre a){
    int b1, b2;
    switch(a->nature){
    case A_OU :
        b1 = evalue_arbre_bool(a->fils_gauche);
        b2 = evalue_arbre_bool(a->fils_gauche->frere_droit);
        return(b1 || b2);
        break;
    case A_ET :
        b1 = evalue_arbre_bool(a->fils_gauche);
        b2 = evalue_arbre_bool(a->fils_gauche->frere_droit);
        return(b1 && b2);
        break;
    case A_NON :
        b1 = evalue_arbre_bool(a->fils_gauche);
        return (!b1);
        break;
    case A_VRAI :
        break;
    case A_FAUX :
        break;
    case A_OPEG :
        b1 = evalue_arbre_bool(a->fils_gauche);
        b2 = evalue_arbre_bool(a->fils_gauche->frere_droit);
        return(b1 = b2);
        break;
    case A_OPDIF :
        b1 = evalue_arbre_bool(a->fils_gauche);
        b2 = evalue_arbre_bool(a->fils_gauche->frere_droit);
        return(b1 != b2);
        break;
    case A_OPINF :
        b1 = evalue_arbre_bool(a->fils_gauche);
        b2 = evalue_arbre_bool(a->fils_gauche->frere_droit);
        return(b1 < b2);
        break;
    case A_OPSUP :
        b1 = evalue_arbre_bool(a->fils_gauche);
        b2 = evalue_arbre_bool(a->fils_gauche->frere_droit);
        return(b1 > b2);
        break;
    case A_OPSUPEG :
        b1 = evalue_arbre_bool(a->fils_gauche);
        b2 = evalue_arbre_bool(a->fils_gauche->frere_droit);
        return(b1 >= b2);
        break;
    case A_OPINFEG :
        b1 = evalue_arbre_bool(a->fils_gauche);
        b2 = evalue_arbre_bool(a->fils_gauche->frere_droit);
        return(b1 <= b2);
        break;
    default :
        break;
    }
    return 0;
}


int evalue_arbre_variable(arbre a){
    return 0;
}

int evalue_arbre_param(){
    return 0;
}

int stocker(){
    return 0;
}



void evalue_arbre(arbre a){
    int pos,val;
    int b;
    switch(a->nature){
    case A_LISTE :
        evalue_arbre(a->fils_gauche);
        evalue_arbre(a->fils_gauche->frere_droit);
        break;
    case A_AFFECT :
        pos = evalue_arbre_variable(a->fils_gauche);
        val = evalue_arbre_exp(a->fils_gauche->frere_droit);
        break;
    case A_IF_THEN_ELSE :
        b = evalue_arbre_bool(a->fils_gauche);
        if(b!=0){
            evalue_arbre(a->fils_gauche->frere_droit);
        }
        else{
            evalue_arbre(a->fils_gauche->frere_droit->frere_droit);
            stocker(val,pos);
        }
    case A_APPEL_PROC :
        evalue_arbre_param();
        
        }
    
        }*/


