#include <stdlib.h>
#include <stdio.h>

#define TAILLE_REPRESENTATION 1000

int representations[TAILLE_REPRESENTATION];

int ajouter_structure (int nbr_champs, int champs[]);

int ajouter_tableaux (int indice, int nb_dimension,int dimensions[]);

int ajouter_fonction (int retour, int nb_param, int param[]);

int ajouter_procedure (int nb_param, int param[]);

void affiche_representation ();