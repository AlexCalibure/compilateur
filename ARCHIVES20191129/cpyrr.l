%{
#include "y.tab.h"
#include "table_lexico.h"
#include "declarations.h"
#include "representation.h"
#define ERREUR 99999
int numligne = 1;
int region = 0;
int indice = 1;
int nbr_champs ;
int nbr_dimensions;
int nbr_params;
int champs[50];
int dimensions[50];
int params[50];


    
%}

%%

\(                        {return (PO);}
\)                        {return (PF);}
\+                        {return(PLUS);}
-                         {return(MOINS);}
\*                        {return(MULT);}
\/                        {return(DIV);}
%                         {return(MOD);}
\{                        {p = empiler(p,region);ajouter_region(region,taille(p));affiche_pile(p);return(DEBUT);}
\}                        {p = depiler(p);affiche_pile(p);return(FIN);}
\;                        {return(PV);}
\:                        {return(DP);}
\[                        {return(CO);}
\]                        {return(CF);}
\,                        {return(V);}
\.                        {return(P);}
\.\.                      {return(P_P);}
->                        {return(FLECHE);}
\|\|                      {return(OU);}
\&\&                      {return(ET);}
=                         {return(OPAFF);}
\<                        {return(OPINF);}
\>                        {return(OPSUP);}
==                        {return(OPEG);}
!=                        {return(OPDIF);}
\<=                       {return(OPINFEG);}
\>=                       {return(OPSUPEG);}
begin                     {init_table_lex();init_declarations();initialiser_region();ajouter_region(region,0); p = init_pile(p); p = empiler(p,region);affiche_pile(p);return(BEG);}
end                       {return(END);}
var                       {return(VARIABLE);}
function                  {region++;return(FONCTION);}
procedure                 {region++;return(PROCEDURE);}
type                      {return(TYPE);;}
struct                    {return(STRUCT);}
fstruct                   {return(FSTRUCT);}
tab                       {return(TABLEAU);}
retour                    {return(RETOURNE);}
void                      {return(VIDE);}
if                        {return (SI);}
then                      {return (ALORS);}
else                      {return (SINON);}
do                        {return(FAIRE);}
while                     {return (TANT_QUE);}
int                       {return(ENTIER);}
float                     {return(REEL);}
bool                      {return(BOOLEEN);}
program                   {return(PROG);}
char                      {return(CARACTERE);}
'[^']'                    {return(UN_CARACTERE);}
\"[^\"]*\"                {return(CHAINE);}
[a-zA-Z][a-zA-Z]*[0-9]*   {yylval = ajouter_lexeme(yytext);return(IDF);}
0|[-]?[1-9][0-9]*         {yylval = atoi(yytext); return CSTE_ENTIERE;}
0|[1-9][0-9]*\.[0-9]+     {yylval = atof(yytext); return CSTE_REELE;}
" "                       ;
\n                        {numligne++;}
.                         {return (ERREUR);}
%%

int yywrap(){
affichage_lexeme();
affiche_declarations();
affiche_representation();
afficher_region();
}