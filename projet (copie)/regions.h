#include<stdlib.h>
#include<stdio.h>

#define TAILLE_MAX 20

typedef struct table_region{
    int taille;
    int NIS;
    int arbre;
} table_regions;

table_regions tab_region[TAILLE_MAX];

typedef struct pile{
    int t_pile[TAILLE_MAX];
    int sommet_pile;
}pile;


pile p;


void initialiser_region();

void ajouter_region(int indice,int nis);

void modifier_taille_region(int indice, int taille);

void afficher_region();

int pile_vide(pile p1);

pile init_pile(pile p1);

pile empiler(pile p1, int region);

pile depiler(pile p1);

int sommet(pile p1);

int taille(pile p1);

pile affiche_pile(pile p1);