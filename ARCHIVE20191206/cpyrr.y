 %{
    #include <stdio.h>
    #include "declarations.h"
    #include "representation.h"
    extern int yyerror();
    extern int yylex();
    extern int numligne;
    extern int region;
    extern int nbr_champs;
    extern int nbr_dimensions;
    extern int nbr_params;
    extern int champs[];
    extern int dimensions[];
    extern int params[];
    extern int indice;
    
%}

%union {
    struct{
        int arbrea;
        int type;
    }struct_arbre;
    int valeur_defaut;
    float valeur_float;
}

%token PROG PV DEBUT FIN TYPE  DP STRUCT FSTRUCT TABLEAU FLECHE ENTIER REEL BOOLEEN CARACTERE CHAINE  RETOURNE PO PF VIDE SI ALORS SINON TANT_QUE FAIRE OPAFF CO CF P V  OU ET NON VRAI FAUX OPEG OPDIF OPINF OPSUP OPINFEG OPSUPEG PLUS MOINS MULT DIV MOD NEG VARIABLE PROCEDURE FONCTION UN_CARACTERE BEG END P_P

%token <valeur_defaut> IDF CSTE_ENTIERE

%token <valeur_flaot> CSTE_REELE

%type <struct_arbre> liste_instructions instruction affectation condition tant_que appel variable ea ea1 ea2

%type <valeur_defaut> liste_declarations liste_declarations_type liste_declarations_variable liste_declarations_pf declaration_type declaration_variable declaration_pf declaration_procedure declaration_fonction suite_declaration_type liste_indices var_struct liste_champs un_champ liste_dimensions une_dimension nom_type type_simple liste_arguments liste_args

%%

programme : PROG  BEG corps END 
          ;

corps : liste_declarations liste_instructions
      ;

liste_declarations : liste_declarations_type liste_declarations_variable liste_declarations_pf
                   ;

liste_declarations_type : liste_declarations_type declaration_type PV
                        | {;}
                        ;

liste_declarations_variable : liste_declarations_variable declaration_variable PV
                            | {;}
                            ;

liste_declarations_pf : liste_declarations_pf declaration_pf PV 
                      | {;}
                      ;

liste_instructions : instruction PV
                   | liste_instructions instruction PV
                   ;

declaration_pf : declaration_fonction 
                | declaration_procedure
                ;


declaration_type : TYPE IDF DP suite_declaration_type {
    if ($4 == STRCT){
        indice = ajouter_declaration($2,$4,sommet(p),ajouter_structure(nbr_champs/3,champs));
    }
      else{
          int tab = ajouter_tableaux($4,nbr_dimensions/2,dimensions);
          printf("TAB %d %d %d\n",$2,sommet(p),tab);
          indice = ajouter_declaration($2,TAB,sommet(p),tab);
          }
      nbr_dimensions = 0;
      nbr_champs = 0;}
                 ;

suite_declaration_type : STRUCT liste_champs FSTRUCT {$$ = STRCT;}
                       | TABLEAU dimension FLECHE nom_type {$$ = $4;}
                       ;

dimension : CO liste_dimensions CF
          ;

liste_dimensions : une_dimension 
                 | liste_dimensions V une_dimension 
                 ;

/* Nous avons vu ce problème en TD... il faut remplacer P P par P_P
pour éviter un conflit par exemple lorsqu'on doit analyser
x.ch1..x.ch2 (relisez le TD concerné)
une_dimension : expression P P expression
              ;
*/

une_dimension : ea P_P ea {dimensions[nbr_dimensions++] = $1.type; dimensions[nbr_dimensions++] = $3.type; $$ = $3.type - $1.type;}
              ;

liste_champs : un_champ 
             | liste_champs PV un_champ
             ;

un_champ : IDF DP nom_type {champs[nbr_champs++] = $3; champs[nbr_champs++] = $1; champs[nbr_champs++] = 1; $$ = $3; }
         ;

nom_type : type_simple {$$ = $1;}
         | IDF {$$ = assoc_nom($1);}
         ;

type_simple : ENTIER {$$ = 0;}
            | REEL {$$ = 1;}
            | BOOLEEN {$$ = 3;}
            | CARACTERE {$$ = 2;}
            | CHAINE CO CSTE_ENTIERE CF {;}
            ;

declaration_variable : VARIABLE IDF DP nom_type {indice = ajouter_declaration($2,VAR,sommet(p),assoc_nom($4),assoc_nom($4));modifier_taille_region(region,tab_declarations[indice].execution);}
                     ;

declaration_procedure : PROCEDURE IDF liste_parametres DEBUT corps FIN {printf("Procedure -> %d\n",region);indice = ajouter_declaration($2,PROC,sommet(p),ajouter_procedure(nbr_params/2,params),region);nbr_params = 0; }
                      ;

declaration_fonction : FONCTION IDF liste_parametres RETOURNE type_simple DEBUT corps FIN {printf("Fonction -> %d\n",region);indice = ajouter_declaration($2,FCT,sommet(p),ajouter_fonction($5,nbr_params/2,params),region);nbr_params=0;}
                     ;

liste_parametres :
                 | PO liste_param PF
		 ;
		 
liste_param : un_param
            | liste_param PV un_param
            ;

un_param : IDF DP type_simple {params[nbr_params++] = $1; params[nbr_params++] = $3;indice = ajouter_declaration($1,PARAM,sommet(p),assoc_nom($3),$3);}
         ;

instruction : affectation
            | condition
            | tant_que
            | appel
            | VIDE {;}
            | RETOURNE resultat_retourne {;}
            ;


resultat_retourne :
                  | expression
		  ;

/* si l'on écrit appel comme vous l'indiquez, cela veut dire qu'on
peut écrire "p" pour appeler une procédure comme instruction
mais comme vous utilisez également appel dans les expressions arithmétique,
lorsqu'on utilise p dans une expression comme (x+2*p)+3 par exemple, et bien l'analyseur syntaxique ne sait pas si p est une variable ou un appel de fonction sans paramètre. Pour solutionner cela, il suffit de dire qu'un appel de fonction, ou de procédure, doit toujours avoir une parenthèse ouvrante et une parenthèse fermante, comme en C. Si on veut appeler un procedure ou fonction p sans paramètre on ndoit écrire p(). Du coup il faut changer la règle de réécriture pour liste_arguments et liste_args */

appel : IDF liste_arguments {;}
      ;

/* votre version
liste_arguments : 
                | PO liste_args PF
    ;

liste_args : un_arg
           | liste_args V un_arg
           ;

*/

/* la version modifiée */
liste_arguments : PO liste_args PF {;}
    ;

liste_args :  {;}
           | un_arg {;}
           | liste_args V un_arg
           ;

un_arg : expression
       ;

condition : SI eb ALORS liste_instructions SINON liste_instructions {;}
          ;

/* comme pour la condition il faut utiliser eb et non pas expression
tant_que : TANT_QUE expression FAIRE liste_instructions
         ;
*/

tant_que : TANT_QUE eb FAIRE liste_instructions {;}
         ;

affectation : variable OPAFF expression {;}
            ;

variable : IDF suite_variable {;}
         ;

suite_variable : CO liste_indices CF var_struct
               | var_struct
	       ;

var_struct : P variable {;}
           | {;}
           ;

liste_indices : ea {;}
              | liste_indices V ea
              ;


expression : ea
           | eb
           ;

eb : eb OU eb1
   | eb1
   ;

eb1 : eb1 ET eb2
    | eb2
    ;

eb2 : NON eb3
    | eb3
    ;

eb3 : PO eb PF
    | VRAI
    | FAUX
    | comparaison
    ;


comparaison : ea OPEG ea
            | ea OPDIF ea
            | ea OPINF ea
            | ea OPSUP ea
            | ea OPINFEG ea
            | ea OPSUPEG ea
            ;

ea : ea PLUS ea1
   | ea MOINS ea1
   | ea1 {$$.type == $1.type;}
   ;

ea1 : ea1 MULT ea2
    | ea1 DIV ea2
    | ea1 MOD ea2
    | ea2 {$$.type = $1.type;}
    ;

ea2 : PO ea PF {;}
    | CSTE_ENTIERE {;} {$$.type = $1;printf("expression %d",$1);}
    | CSTE_REELE {;}
    | CHAINE {;}
    | UN_CARACTERE {;}
    | variable 
    | appel
    ;


%%

int yyerror(){
printf("Erreur de syntaxe en ligne %d\n", numligne);
}
