#include<stdlib.h>
#include<stdio.h>

#define TAILLEMAX 20

typedef struct table_region{
    int taille;
    int NIS;
    int arbre;
} table_regions;

table_regions tab_region[TAILLEMAX];

int pile[TAILLEMAX];



void initialiser_region();

void ajouter_region(int indice, int taille, int NIS , int arbre);

void afficher_region();

void init_pile();

void empiler();

void depiler();

int sommet();

void affiche_pile();