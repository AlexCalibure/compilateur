#include "representation.h"

int indice_representation = 0;

int ajouter_structure (int nbr_champs, int champs[]){
    int i;
    int res = indice_representation;
    representations[indice_representation++] = nbr_champs;
    for( i = 0; i < nbr_champs*3; i=i+3){
        representations[indice_representation++] = champs[i];
        representations[indice_representation++] = champs[i+1];
        representations[indice_representation++] = champs[i+2];
    }
    return res;
}

int ajouter_tableaux (int indice, int nb_dimension, int dimensions[]){
    int i;
    int res = indice_representation;
    representations[indice_representation++] = indice;
    representations[indice_representation++] = nb_dimension;
    for ( i = 0; i < nb_dimension*2; i=i+2){
        representations[indice_representation++] = dimensions[i];
        representations[indice_representation++] = dimensions[i+1];
    }
    return res;
}

int ajouter_fonction (int retour, int nb_param, int param[]){
    int i;
    int res = indice_representation;    
    representations[indice_representation++] = retour;
    representations[indice_representation++] = nb_param;
    for ( i = 0; i < nb_param*2; i = i+2){
        representations[indice_representation++] = param[i];
        representations[indice_representation++] = param[i+1];
    }
    return res;
}

int ajouter_procedure (int nb_param, int param[]){
    int i;
    int res = indice_representation;
    representations[indice_representation++] = nb_param;
    for ( i = 0; i < nb_param*2; i = i+2){
        representations[indice_representation++] = param[i];
        representations[indice_representation++] = param[i+1];
    }
    return res;
}

void affiche_representation(){
    int i;
    printf("\nTable des representation\n|");
    for ( i = 0; i < indice_representation; i++){
        printf("%d | ",representations[i]);
    }
    printf("\n");
}