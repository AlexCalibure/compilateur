#include "declarations.h"
#include "table_lexico.h"
#include <string.h>

void init_declarations (){
    int i;
    for ( i = 0; i < 4; i++){
        tab_declarations[i].nature = BASE;
        tab_declarations[i].suivant = -1;
        tab_declarations[i].region = 0;
        tab_declarations[i].description = i;
        tab_declarations[i].execution = 1;
    }
    for ( i = 4; i < TAILLE; i++){
        tab_declarations[i].nature = RIEN;
        tab_declarations[i].suivant = -1;
        tab_declarations[i].region = -1;
        tab_declarations[i].description = -1;
        tab_declarations[i].execution = -1;
    }
}

int  ajouter_declaration (int indice, int nature, int region, int description, int execution){
    //Si c'est la premiere declaration d'un lexeme.
    int res = 0;
    if (tab_declarations[indice].nature == RIEN ){
        tab_declarations[indice].nature = nature;
        tab_declarations[indice].region = region;
        res = indice;
        tab_declarations[indice].description = description;
        tab_declarations[indice].execution = execution;
    }

    //Si ce n'est pas la premiere declaration du lexeme.
    else{
        printf("Redeclaration de indice %d\n",indice);
        int limite = LIMITE;
        while (tab_declarations[limite].nature != RIEN){   //On cherche un espace libre dans la seconde partie de la table des declarations pour faire la nouvelle declaration.
            limite ++;
            if ( limite == TAILLE ){  //On verifie qu'on ne dépasse pas la taille du tableau.
                printf("Trop de déclaration !\n");
                exit(-1);
            }
        }
        tab_declarations[indice].suivant = limite;
        tab_declarations[limite].nature = nature;
        tab_declarations[limite].region = region;
        res = limite;
        tab_declarations[limite].description = description;
        tab_declarations[indice].execution = execution;
    }
    return res;
}

int chercher_declaration_type (int indice){
    int i = indice;
    while (tab_declarations[i].nature > STRCT)
        i = tab_declarations[i].suivant;
    if (tab_declarations[i].nature == -1)
        return -1;
    return i;
}

void affiche_declarations(){
    int i;
    printf("\nTable des declarations\n__________________________________________________________________________________\n");
    printf("Indice    |    Nature   |  Suivant   |   Region   |   Description  |    Execution\n");
    printf("__________________________________________________________________________________\n");
    for (i = 0; i < TAILLE; i++){
        if(tab_declarations[i].nature != -1)
        printf("%d        |  %d      |    %d       |    %d      |    %d      |                |\n",i,tab_declarations[i].nature, tab_declarations[i].suivant, tab_declarations[i].region, tab_declarations[i].description);
        
    }
}

int assoc_nom(int num_lexico){
    pile p1;
    int i = num_lexico;
    
    init_pile(p1);
    printf("Ok\n");
    if(tab_declarations[i].region == sommet(p)){
        printf("Assoc nom de %s --> %d\n",tb_lex[i].lexem,i);
        return i;
    }
    while (tab_declarations[i].suivant != -1){

        i = tab_declarations[i].suivant;

        if ( tab_declarations[i].region == sommet(p) ){
            while ( !pile_vide(p1) ){
                empiler(p,sommet(p1));
                depiler(p1);
            }
            printf("Assoc nom de %s --> %d\n",tb_lex[i].lexem,i);
            return i;
        }

        empiler(p1,sommet(p));
        depiler(p);
        
        if (pile_vide(p)){
            printf("Aucune declaration de %s\n",tb_lex[num_lexico].lexem); 
            while(!pile_vide(p1)){
                empiler(p,sommet(p1));
                depiler(p1);
            }
            return -1;
        }
        
 
    }
    
    return -1;
}

    