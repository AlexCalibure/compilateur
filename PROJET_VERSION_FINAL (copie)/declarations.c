#include "declarations.h"
#include "table_lexico.h"
#include "representation.h"
#include <string.h>
#include <stdarg.h>

void init_declarations (){
    int i;
    //Boucle pour remplir le tableau des déclarations avec les types de base
    for ( i = 0; i < 4; i++){
        tab_declarations[i].nature = BASE;
        tab_declarations[i].suivant = -1;
        tab_declarations[i].region = 0;
        tab_declarations[i].description = i;
        tab_declarations[i].execution = 1;
    }
    //Boucle pour initialiser toutes les autres cases du tableaux à -1
    for ( i = 4; i < TAILLE; i++){
        tab_declarations[i].nature = RIEN;
        tab_declarations[i].suivant = -1;
        tab_declarations[i].region = -1;
        tab_declarations[i].description = -1;
        tab_declarations[i].execution = -1;
    }
}

/* les params "indice" et "type" sont les indices de l'objet et de son type dans le lexem/declaration  */
int calcule_exec(int indice, int type){
    int nature = tab_declarations[indice].nature;
    int description = tab_declarations[indice].description;
    int i;
    int execution = 0;

    // calcule de la taille pour le champ execution en fonction de la nature de la var
    switch(nature){
        // type de base vaut 1
    case BASE:
        return 1;
        break;
    // type STRUCURE on parcour le tableau de declaration on recupere le type de chaques champs
    case STRCT:
        for(i=description+1; i<(representations[description]*3)+description; i+=3){
            //type de base on ajoute 1
            if(representations[i] >= 0 && representations[i] <4){
                execution++;
            }
            // type autre on recupere la valeur de execution de celui ci
            else{
                execution += tab_declarations[representations[i]].execution;
            }
        }
        break;
    // type TABLEAU = type_element * nombre_dimension * (born_sup-born_inf)
    case TAB:
        execution += tab_declarations[representations[description]].execution;
        
        /* pour chaque dimension  */
        for(i=description+2; i<(representations[description+1]*2)+description+1; i+=2){
            // borne sup - borne inf +1 pour compter lelem de depart
            execution *= (representations[i+1] - representations[i])+1;
        }
        break;
        // cherche l'execution du type de la variable
    case VAR:
        execution += tab_declarations[type].execution;
        break;
    case PARAM:
        execution += tab_declarations[type].execution;
        break;
        // execution = numero region qui est cree par la procedure
    case PROC:
        execution += type;
        break;
        // execution = numero region qui est cree par la fonction
    case FCT:
        execution += type;
        break;
    default:
        printf("probleme switch declaration.c fonctin calcule exec\n");     
    }
    return execution;
}

/* fonction taille variable pour gerer les ajout de variable un argument suplementaire 
   possible un int qui contion l'indice dans a table lexico du type de la variable */

int  ajouter_declaration (int indice, int nature, int region, int description,...){
    //Si c'est la premiere declaration d'un lexeme.
    int res = 0;
    
    va_list indice_type;
    va_start(indice_type, description);
    
    if (tab_declarations[indice].nature == RIEN ){
        tab_declarations[indice].nature = nature;
        tab_declarations[indice].region = region;
        res = indice;
        tab_declarations[indice].description = description;
        tab_declarations[indice].execution = calcule_exec(indice, va_arg(indice_type,int));
    }

    //Si ce n'est pas la premiere declaration du lexeme.
    else{
        int limite = LIMITE;
        while (tab_declarations[limite].nature != RIEN){   //On cherche un espace libre dans la seconde partie de la table des declarations pour faire la nouvelle declaration.
            limite ++;
            if ( limite == TAILLE ){  //On verifie qu'on ne dépasse pas la taille du tableau.
                printf("Trop de déclaration !\n");
                exit(-1);
            }
        }
        tab_declarations[indice].suivant = limite;
        tab_declarations[limite].nature = nature;
        tab_declarations[limite].region = region;
        res = limite;
        tab_declarations[limite].description = description;
        tab_declarations[limite].execution = calcule_exec(indice,va_arg(indice_type, int));
    }

    va_end(indice_type);
    return res;
}

//Fonction d'affichage de la table des declarations
void affiche_declarations(){
    int i;
    char nature[10]; // utiliser pour afficher la nature
    
    printf("\nTable des declarations\n__________________________________________________________________________________\n");
    printf("Indice    |    Nature   |  Suivant   |   Region   |   Description  |    Execution\n");
    printf("__________________________________________________________________________________\n");
    
    for (i = 0; i < TAILLE; i++){
        if(tab_declarations[i].nature != -1){
            // choisie le nom a afficher en fonction du numero de nature
            switch(tab_declarations[i].nature){
            case -1:
                strcpy(nature,"rien ");
                break;
            case 0:
                strcpy(nature,"base ");
                break;
            case 1:
                strcpy(nature,"strct");
                break;
            case 2:
                strcpy(nature,"tab  ");
                break;
            case 3:
                strcpy(nature,"var  ");
                break;
            case 4:
                strcpy(nature,"param");
                break;
            case 5:
                strcpy(nature,"proc ");
                break;
            case 6:
                strcpy(nature,"fct  ");
                break;
            default:
                sprintf(nature,"%d    ",tab_declarations[i].nature); // affiche le num pour les natures speciales
                break;
            }
            
        printf("%d        |  %s      |    %d       |    %d      |    ",i,nature, tab_declarations[i].suivant, tab_declarations[i].region);
        if(tab_declarations[i].description == RIEN)
            printf("rien     |       %d         |\n",tab_declarations[i].execution);
        else
            printf("%d      |       %d         |\n",tab_declarations[i].description,tab_declarations[i].execution);
        }
    }
}

//Fonction d'association de nom renvoyant le numero de declaration d'une variable
int assoc_nom(int num_lexico){
    pile p1=init_pile(p1);
    int i = num_lexico;
    
    switch(i){
    case (0):
        return 0;
    case (1):
        return 1;
    case(2):
        return 2;
    case (3):
        return 3;
    }


    if(tab_declarations[i].nature != RIEN){
        if(tab_declarations[i].region == sommet(p)){
            return i;
        }
        while( !pile_vide(p)){

            if ( tab_declarations[i].region == sommet(p) ){
                while ( !pile_vide(p1) ){
                    p=empiler(p,sommet(p1));
                    p1=depiler(p1);
                }
                return i;
            }
            while (tab_declarations[i].suivant != -1){
            
                i = tab_declarations[i].suivant;

                if ( tab_declarations[i].region == sommet(p) ){
                    while ( !pile_vide(p1) ){
                        p=empiler(p,sommet(p1));
                        p1=depiler(p1);
                    }
                    return i;
                }
            }
            
            /* printf("Affichage de la pile 1 fois\n");
            affiche_pile(p);
            affiche_pile(p1);*/
            p1=empiler(p1,sommet(p));
            p=depiler(p);
            /* printf("Affichage de la pile 2 fois\n");
            affiche_pile(p);
            affiche_pile(p1);*/
        }

        while(!pile_vide(p1)){
            p=empiler(p,sommet(p1));
            p1=depiler(p1);
        }
        return -1;
    } 
    return -1;       
}
  