%{
#include "y.tab.h"
#include "table_lexico.h"
#include "declarations.h"
#define ERREUR 99999
int numligne = 1;
int region = 1;
%}

%%

\(                        {return (PO);}
\)                        {return (PF);}
\+                        {return(PLUS);}
-                         {return(MOINS);}
\*                        {return(MULT);}
\/                        {return(DIV);}
%                         {return(MOD);}
\{                        {region++;return(DEBUT);}
\}                        {region--;return(FIN);}
\;                        {return(PV);}
\:                        {return(DP);}
\[                        {return(CO);}
\]                        {return(CF);}
\,                        {return(V);}
\.                        {return(P);}
\.\.                      {return(P_P);}
->                        {return(FLECHE);}
\|\|                      {return(OU);}
\&\&                      {return(ET);}
=                         {return(OPAFF);}
\<                        {return(OPINF);}
\>                        {return(OPSUP);}
==                        {return(OPEG);}
!=                        {return(OPDIF);}
\<=                       {return(OPINFEG);}
\>=                       {return(OPSUPEG);}
begin                     {init_table_lex();init_declarations();return(BEG);}
end                       {affichage_lexeme();affiche_declarations();return(END);}
var                       {return(VARIABLE);}
function                  {return(FONCTION);}
procedure                 {return(PROCEDURE);}
type                      {return(TYPE);}
struct                    {return(STRUCT);}
fstruct                   {return(FSTRUCT);}
tab                       {return(TABLEAU);}
retour                    {return(RETOURNE);}
void                      {return(VIDE);}
if                        {return (SI);}
then                      {return (ALORS);}
else                      {return (SINON);}
do                        {return(FAIRE);}
while                     {return (TANT_QUE);}
int                       {return(ENTIER);}
float                     {return(REEL);}
bool                      {return(BOOLEEN);}
program                   {return(PROG);}
char                      {return(CARACTERE);}
'[^']'                    {return(UN_CARACTERE);}
\"[^\"]*\"                {return(CHAINE);}
[a-zA-Z][a-zA-Z]*[0-9]*   {yylval = ajouter_lexeme(yytext);return(IDF);}
0|[-]?[1-9][0-9]*         {yylval = atoi(yytext); return CSTE_ENTIERE;}
0|[1-9][0-9]*\.[0-9]+     {yylval = atof(yytext); return CSTE_REELE;}
" "                       ;
\n                        {numligne++;}
.                         {return (ERREUR);}
%%
