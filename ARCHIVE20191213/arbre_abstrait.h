#ifndef _arbre_abstrait_h_
#define _arbre_abstrait_h_

#include<stdlib.h>
#include<stdio.h>

// NULL
#define EMPTY -1

// type simple
#define A_INT 0
#define A_FLOAT 1
#define A_CHAR 2
#define A_BOOL 3
#define A_STRING 4

// operation
#define A_IDF 5
#define A_MULT 6
#define A_DIV 7
#define A_MOD 8
#define A_MOINS 9
#define A_PLUS 10

// op binaire
#define A_OPAFF 11
#define A_OPEG 12
#define A_OPDIF 13
#define A_OPINF 14
#define A_OPSUP 15
#define A_OPINFEG 16
#define A_OPSUPEG 17
#define A_FAUX 18
#define A_VRAI 19
#define A_OU 20
#define A_ET 21
#define A_NON 22
#define A_FONCTION 23
#define A_PROCEDURE 24

#define A_LISTE_INDICE 25
#define A_LISTE_CHAMPS 26
#define A_STRUCT 27
#define A_TAB 28
#define A_LISTE_ARGS 29
#define A_LISTE_INST 30
#define A_AFFECT 31
#define A_APPEL_PROC 32
#define A_IF_THEN_ELSE 33
#define A_WHILE 34
#define A_APPEL 35
#define A_VIDE 36
#define A_RETURN 37
#define A_CHAINE 38

typedef struct noeud {
    int nature;
    int num_lex;
    int num_dec;
    struct noeud* fils_gauche;
    struct noeud* frere_droit;
} noeud;

typedef noeud *arbre;

/*
union cellule{
    int entier ;
    float reel ;
    char boleen ;
    char caractere ;
} cellule ;

cellule pile[5000] ;
*/

int est_vide();

arbre creer_arbre_vide();
 
arbre creer_noeud (int nature, int num_lex, int num_dec);
  
arbre creer_arbre (int nature, arbre filsgauche, arbre frere_droit);

arbre concat_pere_fils (arbre a, arbre filsgauche);

arbre concat_fils_frere (arbre a, arbre frere_droit);

void afficher_arbre(arbre a);

void evalue_arbre(arbre  a);

int evalue_arbre_exp(arbre  a);

int evalue_arbre_bool(arbre a);

int evalue_arbre_variable(arbre a);

int evalue_arbre_param();

int stocker();


#endif

