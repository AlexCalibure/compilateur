%{
#include "y.tab.h"
#include "table_lexico.h"
#include "declarations.h"
#include "representation.h"
#include "regions.h"
#define ERREUR 99999
int numligne = 1;
int region = 1;
int taille = 0;
int indice = 1;
int nbr_champs ;
int nbr_dimensions;
int nbr_params;
int champs[50];
int dimensions[50];
int params[50];

    
%}

%%

\(                        {return (PO);}
\)                        {return (PF);}
\+                        {return(PLUS);}
-                         {return(MOINS);}
\*                        {return(MULT);}
\/                        {return(DIV);}
%                         {return(MOD);}
\{                        {/*ajouter_region(region,taille,region,0);*/region++;taille=0;empiler();affiche_pile();return(DEBUT);}
\}                        {region--;ajouter_region(region,taille,region-1,0); depiler();affiche_pile();return(FIN);}
\;                        {return(PV);}
\:                        {return(DP);}
\[                        {return(CO);}
\]                        {return(CF);}
\,                        {return(V);}
\.                        {return(P);}
\.\.                      {return(P_P);}
->                        {return(FLECHE);}
\|\|                      {return(OU);}
\&\&                      {return(ET);}
=                         {return(OPAFF);}
\<                        {return(OPINF);}
\>                        {return(OPSUP);}
==                        {return(OPEG);}
!=                        {return(OPDIF);}
\<=                       {return(OPINFEG);}
\>=                       {return(OPSUPEG);}
begin                     {init_table_lex();init_declarations();initialiser_region();ajouter_region(0,taille,0,-1);init_pile();return(BEG);}
end                       {affichage_lexeme();affiche_declarations();affiche_representation();afficher_region();return(END);}
var                       {taille += 1;return(VARIABLE);}
function                  {return(FONCTION);}
procedure                 {return(PROCEDURE);}
type                      {taille+=1; return(TYPE);}
struct                    {taille += nbr_champs;return(STRUCT);}
fstruct                   {return(FSTRUCT);}
tab                       {taille += nbr_dimensions;return(TABLEAU);}
retour                    {return(RETOURNE);}
void                      {return(VIDE);}
if                        {return (SI);}
then                      {return (ALORS);}
else                      {return (SINON);}
do                        {return(FAIRE);}
while                     {return (TANT_QUE);}
int                       {return(ENTIER);}
float                     {return(REEL);}
bool                      {return(BOOLEEN);}
program                   {return(PROG);}
char                      {return(CARACTERE);}
'[^']'                    {return(UN_CARACTERE);}
\"[^\"]*\"                {return(CHAINE);}
[a-zA-Z][a-zA-Z]*[0-9]*   {yylval = ajouter_lexeme(yytext);return(IDF);}
0|[-]?[1-9][0-9]*         {yylval = atoi(yytext); return CSTE_ENTIERE;}
0|[1-9][0-9]*\.[0-9]+     {yylval = atof(yytext); return CSTE_REELE;}
" "                       ;
\n                        {numligne++;}
.                         {return (ERREUR);}
%%
