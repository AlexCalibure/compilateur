#include "declarations.h"
#include <string.h>

void init_declarations (declarations tab_declarations[]){
    int i;
    for ( i = 0; i < TAILLE; i++){
        tab_declarations[i].nature = malloc(30*sizeof(char));
        tab_declarations[i].nature = "Vide";
        tab_declarations[i].suivant = -1;
        tab_declarations[i].region = 0;
        tab_declarations[i].description = -1;
        //tab_declarations[i].execution = 0;
    }
}

void ajouter_declaration (declarations tab_declarations[], int indice, char* nature, int region, int description/*, int execution*/ ){
    //Si c'est la premiere declaration d'un lexeme.
    if ( strcmp(tab_declarations[indice].nature,"Vide") == 0 ){
        tab_declarations[indice].nature = nature;
        tab_declarations[indice].region = region;
        tab_declarations[indice].description = description;
        //tab_declarations[indice].execution = execution;
    }

    //Si ce n'est pas la premiere declaration du lexeme.
    else{
        int limite = LIMITE;
        while (tab_declarations[limite].nature != NULL){   //On cherche un espace libre dans la seconde partie de la table des declarations pour faire la nouvelle declaration.
            limite ++;
            if ( limite == TAILLE ){  //On verifi qu'on ne dépasse pas la taille du tableau.
                printf("Trop de déclaration !\n");
                exit(-1);
            }
        }
        strcpy(tab_declarations[limite].nature,nature);
        tab_declarations[limite].region = region;
        tab_declarations[limite].description = description;
        //tab_declarations[indice].execution = execution;
        limite ++;
    }
}


void affichage (declarations tab_declarations[]){
    int i;
    printf("Nature     Suivant   Region    Description Execution\n");
    for (i = 0; i < TAILLE; i++){
        printf("  %s       %d       %d          %d\n",tab_declarations[i].nature, tab_declarations[i].suivant, tab_declarations[i].region, tab_declarations[i].description);
    }
}

int main(){
    declarations tab_declarations[TAILLE];
    init_declarations(tab_declarations);
    ajouter_declaration(tab_declarations,0,"int",1,10);
    ajouter_declaration(tab_declarations,1,"float",1,15);
    ajouter_declaration(tab_declarations,2,"char",1,4);
    affichage(tab_declarations);
    exit(1);
}