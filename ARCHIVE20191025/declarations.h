#include <stdlib.h>
#include <stdio.h>

#define TAILLE 510
#define LIMITE 200
#define RIEN -1
#define BASE 0
#define STRCT 1
#define TAB 2
#define VAR 3
#define PARAM 4
#define PROC 5
#define FCT 6

typedef struct declaration {
    int nature;
    int suivant;
    int region;
    int description;
    //int execution;
}declarations;

declarations tab_declarations[509];

void init_declarations();

void ajouter_declaration(int indice, int nature, int region);

void affiche_declarations();